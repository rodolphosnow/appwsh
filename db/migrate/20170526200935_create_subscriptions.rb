class CreateSubscriptions < ActiveRecord::Migration[5.0]
  def change
    create_table :subscriptions do |t|
      t.references :user,    index: true
      t.references :company, index: true
      t.references :plan,    index: true
      t.decimal    :total,   precision: 14, scale: 2, default: 0
      t.string     :gateway_id
      t.string     :status
      t.string     :slug
      t.string     :code
      t.date       :init
      t.date       :ends
      t.timestamps
    end
  end
end
