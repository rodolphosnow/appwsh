class CreateInvoices < ActiveRecord::Migration[5.0]
  def change
    create_table :invoices do |t|
      t.references :subscription, index: true
      t.string :total
      t.string :description
      t.string :invoice_id
      t.string :subscription_id
      t.string :status, default: "aguardando"
      t.timestamps
    end
  end
end
