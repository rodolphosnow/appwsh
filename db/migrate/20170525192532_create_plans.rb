class CreatePlans < ActiveRecord::Migration[5.0]
  def change
    create_table :plans do |t|
      t.string  :title
      t.text    :description
      t.decimal :price, precision: 14, scale: 2, default: 0
      t.string  :size, default: "medium"
      # To control plan limits
      t.integer :complete, default: 1
      t.integer :partial,  default: 1
      t.boolean :active,   default: true
      t.string  :slug,     index: true

      t.timestamps
    end
  end
end
