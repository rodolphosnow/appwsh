class CreateCompanyRatings < ActiveRecord::Migration[5.0]
  def change
    create_table :company_ratings do |t|
      t.references :company, index: true
      t.decimal :internal, default: 3
      t.decimal :external, default: 0
      t.string  :message
      t.timestamps
    end
  end
end
