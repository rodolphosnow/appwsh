class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.references :user,    index: true
      t.references :company, index: true
      t.string  :street
      t.string  :number
      t.string  :neighborhood
      t.string  :complement
      t.string  :zipcode
      t.string  :city
      t.string  :state
      t.string  :country, default: 'Brasil'
      # To keep the storage space required for your table at a minimum, you can specify that the lat and lng attributes are floats of size (10,6).
      t.decimal :latitude,  precision: 10, scale: 6, index: true
      t.decimal :longitude, precision: 10, scale: 6, index: true

      t.boolean :active, default: true
      t.timestamps
    end
  end
end
