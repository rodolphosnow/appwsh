class CreateCompanyServices < ActiveRecord::Migration[5.0]
  def change
    create_table :company_services do |t|
      t.references :company, index: true
      t.references :service, index: true
      t.decimal    :price,   precision: 14, scale: 2, default: 0
      t.timestamps
    end
  end
end
