class CreateServices < ActiveRecord::Migration[5.0]
  def change
    create_table :services do |t|
      t.string  :name
      t.text    :description
      t.decimal :price,  precision: 14, scale: 2, default: 0
      t.boolean :extra,  default: false
      t.boolean :active, default: true
      t.string  :slug,   index: true
      t.timestamps
    end
  end
end
