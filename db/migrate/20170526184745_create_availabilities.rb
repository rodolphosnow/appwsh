class CreateAvailabilities < ActiveRecord::Migration[5.0]
  def change
    create_table :availabilities do |t|
      t.string  :week_day
      t.integer :init,   default: 8
      t.integer :ends,   default: 18
      t.boolean :active, default: true
      t.string  :slug,   index: true
      t.string  :code
      t.timestamps
    end
  end
end
