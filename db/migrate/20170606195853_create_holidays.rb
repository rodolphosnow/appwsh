class CreateHolidays < ActiveRecord::Migration[5.0]
  def change
    create_table :holidays do |t|
      t.date :date
      t.references :state,  index: true
      t.references :city,   index: true
      t.boolean    :active, default: true
      t.string     :slug,   index: true
      t.string     :code
      t.timestamps
    end
  end
end
