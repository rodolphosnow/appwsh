class CreateUserVehicles < ActiveRecord::Migration[5.0]
  def change
    create_table :user_vehicles do |t|
      t.references :user,    index: true
      t.references :brand,   index: true
      t.references :vehicle, index: true
      t.string  :identifier
      t.string  :year
      t.string  :model
      t.boolean :active, default: true
      t.string  :slug, index: true

      t.timestamps
    end
  end
end
