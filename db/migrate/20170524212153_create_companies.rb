class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      ## Database authenticatable
      t.attachment :image
      t.string :name
      t.string :email,              null: false, default: ""
      t.string :identifier
      t.string :registration
      t.text :description
      t.string :encrypted_password, null: false, default: ""
      t.string :phone
      t.string :mobile
      t.integer :company_ratings_count
      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.inet     :current_sign_in_ip
      t.inet     :last_sign_in_ip

      ## Confirmable
      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      # t.integer  :failed_attempts, default: 0, null: false # Only if lock strategy is :failed_attempts
      # t.string   :unlock_token # Only if unlock strategy is :email or :both
      # t.datetime :locked_at

      t.integer :limit_per_hour, default: 2
      t.boolean :active, default: true
      t.references :city, index: true
      t.string  :slug, index: true

      t.timestamps null: false
    end

    add_index :companies, :email,                unique: true
    add_index :companies, :reset_password_token, unique: true
    # add_index :companies, :confirmation_token,   unique: true
    # add_index :companies, :unlock_token,         unique: true
  end
end
