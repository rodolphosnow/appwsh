class CreateBrands < ActiveRecord::Migration[5.0]
  def change
    create_table :brands do |t|
      t.string  :name
      t.integer :code
      t.boolean :active, default: true
      t.string  :slug, index: true
      t.timestamps
    end
  end
end
