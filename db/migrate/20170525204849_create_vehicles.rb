class CreateVehicles < ActiveRecord::Migration[5.0]
  def change
    create_table :vehicles do |t|
      t.references :brand, index: true
      t.string :name
      t.string :model
      t.string :year
      t.string :size,    default: 'medium'
      t.boolean :active, default: true
      t.string :slug,    index: true

      t.timestamps
    end
  end
end
