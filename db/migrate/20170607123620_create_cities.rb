class CreateCities < ActiveRecord::Migration[5.0]
  def change
    create_table :cities do |t|
      t.string     :name
      t.references :state,   index: true
      t.boolean    :capital, default: false
      t.boolean    :active,  default: true
      t.string     :slug,    index: true
      t.timestamps
    end
  end
end
