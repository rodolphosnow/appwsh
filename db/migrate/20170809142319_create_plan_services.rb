class CreatePlanServices < ActiveRecord::Migration[5.0]
  def change
    create_table :plan_services do |t|
      t.references :plan, index: true
      t.references :service, index: true
      t.integer :quantity
      t.string :slug
      t.string :code
      t.timestamps
    end
  end
end
