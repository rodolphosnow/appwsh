class CreateStates < ActiveRecord::Migration[5.0]
  def change
    create_table :states do |t|
      t.string  :name
      t.string  :uf, limit: 2
      t.string  :country
      t.boolean :active, default: true
      t.string  :slug,   index: true

      t.timestamps
    end
  end
end
