class CreateUserSchedules < ActiveRecord::Migration[5.0]
  def change
    create_table :user_schedules do |t|
      t.references :user,    index: true
      t.references :company, index: true
      t.string   :services
      t.string   :extras
      t.datetime :date
      t.datetime :expiration
      t.integer  :state, status: 0
      t.datetime :finished_at
      t.datetime :cancelled_at
      t.string   :slug
      t.string   :code
      t.timestamps
    end
  end
end
