# encoding: utf-8

puts '+ CITIES AND STATES'

file = File.read("#{ Rails.root }/public/data/brazil-cities-states.json")
data = JSON.parse(file)

capitals = ["Rio Branco", "Maceió", "Macapá", "Manaus", "Salvador", "Fortaleza", "Brasília", "Vitória", "Goiânia", "São Luís", "Cuiabá", "Campo Grande", "Belo Horizonte", "Belém", "João Pessoa", "Curitiba", "Recife", "Teresina", "Rio de Janeiro", "Natal", "Porto Alegre", "Porto Velho", "Boa Vista", "Florianópolis", "São Paulo", "Aracaju" ,"Palmas"]
data['estados'].each_with_index do |attrs, i|
  state = State.where(name: attrs['nome'], uf: attrs['sigla']).first_or_create
  attrs['cidades'].each do |city|
    c = state.cities.where(name: city, capital: capitals.include?(city)).first_or_create
    puts [c.name, c.state.name, c.state.uf].join(', ')
  end
end
