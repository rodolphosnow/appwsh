# encoding: utf-8


ducha_rapida = Service.create(name: 'Ducha rápida', description: 'Lorem ipsum sit amet dolor')
lavagem_completa = Service.create(name: 'Lavagem completa', description: 'Lorem ipsum sit amet dolor')


puts '+ PLANS'

plan = Plan.create(title: 'CARWASH SMART',   description: '1 lavagem completa e 1 ducha', price: 39.90, size: "medium")
plan.plan_services.build(service_id: lavagem_completa.id, quantity: 1)
plan.plan_services.build(service_id: ducha_rapida.id, quantity: 1)
plan.save
puts plan


plan = Plan.create(title: 'CARWASH SMART G',   description: '1 lavagem completa e 1 ducha', price: 49.90, size: "large")
plan.plan_services.build(service_id: lavagem_completa.id, quantity: 1)
plan.plan_services.build(service_id: ducha_rapida.id, quantity: 1)
plan.save
puts plan

plan = Plan.create(title: 'CARWASH PLUS',    description: '2 lavagens completas', price: 49.90, size: "medium")
plan.plan_services.build(service_id: lavagem_completa.id, quantity: 2)
plan.save
puts plan

plan = Plan.create(title: 'CARWASH PLUS G',    description: '2 lavagens completas', price: 59.90, size: "large")
plan.plan_services.build(service_id: lavagem_completa.id, quantity: 2)
plan.save
puts plan

plan =  Plan.create(title: 'CARWASH MASTER',  description: '1 lavagem completa e 2 duchas', price: 59.90, size: "medium")
plan.plan_services.build(service_id: lavagem_completa.id, quantity: 1)
plan.plan_services.build(service_id: ducha_rapida.id, quantity: 2)
plan.save
puts plan

plan =  Plan.create(title: 'CARWASH MASTER G',  description: '1 lavagem completa e 2 duchas', price: 69.90, size: "large")
plan.plan_services.build(service_id: lavagem_completa.id, quantity: 1)
plan.plan_services.build(service_id: ducha_rapida.id, quantity: 2)
plan.save
puts plan

plan = Plan.create(title: 'CARWASH PREMIUM', description: '2 lavagens completa e 2 duchas', price: 79.90, size: "medium")
plan.plan_services.build(service_id: lavagem_completa.id, quantity: 2)
plan.plan_services.build(service_id: ducha_rapida.id, quantity: 2)
plan.save
puts plan

plan = Plan.create(title: 'CARWASH PREMIUM G', description: '2 lavagens completa e 2 duchas', price: 89.90, size: "large")
plan.plan_services.build(service_id: lavagem_completa.id, quantity: 2)
plan.plan_services.build(service_id: ducha_rapida.id, quantity: 2)
plan.save
puts plan
