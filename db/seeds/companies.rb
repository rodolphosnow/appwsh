# encoding: utf-8

puts '+ COMPANIES'

companies = ['Lava car. Jd. Botanico', 'Lava car Barigui', 'Lava car São José', 'Lava car Lindóia']
companies.each do |company|
  puts Company.create(name: company, email: "#{company.parameterize.gsub('-', '_')}@lavacar.com.br", identifier: CNPJ.generate(true), registration: CPF.generate, description: "Lorem ipsum sit dolor amet", phone: "(41) 3222-1000", mobile: nil, limit_per_hour: 2, active: true, password: '909090')
end

Company.create(name: "company 1", email: "empresa@lavacar.com.br", identifier: CNPJ.generate(true), registration: CPF.generate, description: "Lorem ipsum sit dolor amet", phone: "(41) 3222-1000", mobile: nil, limit_per_hour: 2, active: true, password: '00000000')

company = Company.find(1)
company.address = Address.create(street: 'R. Engenheiro Ostoja Roguski', number: '690', neighborhood: 'Jardim Botânico', zipcode: '80210-390', city: 'Curitiba', state: 'PR', country: 'Brasil')

company = Company.find(2)
company.address = Address.create(street: 'Av. Cândido Hartmann', number: '2950', neighborhood: 'Mercês', zipcode: '80710-570', city: 'Curitiba', state: 'PR', country: 'Brasil')

company = Company.find(3)
company.address = Address.create(street: 'Rua Dona Izabel A Redentora', number: '1434', neighborhood: 'Centro', zipcode: '83005-010', city: 'São José dos Pinhais', state: 'PR', country: 'Brasil')

company = Company.find(4)
company.address = Address.create(street: "Rua Roberto Koch", number: "220", neighborhood: "Lindóia", zipcode: "81010-220", city: "Curitiba", state: "PR", country: "Brasil")

puts '+ COMPANIES AVAILABILITIES'


# Company.all.each do |company|
#   Availability.all.each do |availability|
#      puts company.company_availabilities.create(availability: availability)
#   end
# end
week_days = %w(SEG TER QUA QUI SEX SAB DOM)
Company.all.each do |company|
  week_days.each do |day|
    ends = day.eql?('SAB') ? 12 : 18
    puts company.availabilities.create(week_day: day, init: 8, ends: ends)
  end
end
