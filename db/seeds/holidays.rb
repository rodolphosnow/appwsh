# encoding: utf-8

puts '+ HOLIDAYS'

city = City.find_by_slug('curitiba')
puts Holiday.create(date: Date.parse('01/09/2017'), city: city, state: city.state)
puts Holiday.create(date: Date.parse('12/10/2017'), city: city, state: city.state)
puts Holiday.create(date: Date.parse('02/11/2017'), city: city, state: city.state)

puts Holiday.create(date: (Date.today + 5.days),  city: city, state: city.state)
puts Holiday.create(date: (Date.today + 15.days), city: city, state: city.state)
