# encoding: utf-8
# https://fipe-parallelum.rhcloud.com/api/v1/carros/marcas

puts '+ BRANDS'

file = File.read("#{ Rails.root }/public/data/brands.json")
data = JSON.parse(file)

data.each do |attrs|
  puts Brand.create(name: attrs['nome'], code: attrs['codigo'])
end

codes = Brand.all.collect(&:code)
# https://fipe-parallelum.rhcloud.com/api/v1/carros/marcas/:code/modelos
# require 'net/http'
#
# puts 'HTTP REQUEST RUNNING...'
# codes.each do |code|
#   file     = File.new("#{ Rails.root }/public/data/brands_#{ code }_models.json", "w+")
#   response = Net::HTTP.get_response(URI("https://fipe-parallelum.rhcloud.com/api/v1/carros/marcas/#{ code }/modelos"))
#   file.write(response.body); file.close;
#   sleep(1); puts Net::HTTPSuccess;
# end

puts '+ VEHICLES'
codes.each do |code|
  file = File.read("#{ Rails.root }/public/data/brands_#{ code }_models.json")
  data = JSON.parse(file)
  data['modelos'].each do |attrs|
    puts Vehicle.create(name: attrs['nome'], size: 'medium', brand: Brand.find_by_code(code))
  end
end
