# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170823163658) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "company_id"
    t.string   "street"
    t.string   "number"
    t.string   "neighborhood"
    t.string   "complement"
    t.string   "zipcode"
    t.string   "city"
    t.string   "state"
    t.string   "country",                               default: "Brasil"
    t.decimal  "latitude",     precision: 10, scale: 6
    t.decimal  "longitude",    precision: 10, scale: 6
    t.boolean  "active",                                default: true
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.index ["company_id"], name: "index_addresses_on_company_id", using: :btree
    t.index ["latitude"], name: "index_addresses_on_latitude", using: :btree
    t.index ["longitude"], name: "index_addresses_on_longitude", using: :btree
    t.index ["user_id"], name: "index_addresses_on_user_id", using: :btree
  end

  create_table "admins", force: :cascade do |t|
    t.string   "name"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  end

  create_table "availabilities", force: :cascade do |t|
    t.string   "week_day"
    t.integer  "init",       default: 8
    t.integer  "ends",       default: 18
    t.boolean  "active",     default: true
    t.string   "slug"
    t.string   "code"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["slug"], name: "index_availabilities_on_slug", using: :btree
  end

  create_table "brands", force: :cascade do |t|
    t.string   "name"
    t.integer  "code"
    t.boolean  "active",     default: true
    t.string   "slug"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["slug"], name: "index_brands_on_slug", using: :btree
  end

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.integer  "state_id"
    t.boolean  "capital",    default: false
    t.boolean  "active",     default: true
    t.string   "slug"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["slug"], name: "index_cities_on_slug", using: :btree
    t.index ["state_id"], name: "index_cities_on_state_id", using: :btree
  end

  create_table "companies", force: :cascade do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "name"
    t.string   "email",                  default: "",   null: false
    t.string   "identifier"
    t.string   "registration"
    t.text     "description"
    t.string   "encrypted_password",     default: "",   null: false
    t.string   "phone"
    t.string   "mobile"
    t.integer  "company_ratings_count"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "limit_per_hour",         default: 2
    t.boolean  "active",                 default: true
    t.integer  "city_id"
    t.string   "slug"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["city_id"], name: "index_companies_on_city_id", using: :btree
    t.index ["email"], name: "index_companies_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_companies_on_reset_password_token", unique: true, using: :btree
    t.index ["slug"], name: "index_companies_on_slug", using: :btree
  end

  create_table "company_availabilities", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "week_day"
    t.integer  "init",       default: 8
    t.integer  "ends",       default: 18
    t.boolean  "active",     default: true
    t.string   "slug"
    t.string   "code"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["company_id"], name: "index_company_availabilities_on_company_id", using: :btree
    t.index ["slug"], name: "index_company_availabilities_on_slug", using: :btree
  end

  create_table "company_ratings", force: :cascade do |t|
    t.integer  "company_id"
    t.decimal  "internal",   default: "3.0"
    t.decimal  "external",   default: "0.0"
    t.string   "message"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["company_id"], name: "index_company_ratings_on_company_id", using: :btree
  end

  create_table "company_services", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "service_id"
    t.decimal  "price",      precision: 14, scale: 2, default: "0.0"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.index ["company_id"], name: "index_company_services_on_company_id", using: :btree
    t.index ["service_id"], name: "index_company_services_on_service_id", using: :btree
  end

  create_table "company_users", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_company_users_on_company_id", using: :btree
    t.index ["user_id"], name: "index_company_users_on_user_id", using: :btree
  end

  create_table "holidays", force: :cascade do |t|
    t.date     "date"
    t.integer  "state_id"
    t.integer  "city_id"
    t.boolean  "active",     default: true
    t.string   "slug"
    t.string   "code"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["city_id"], name: "index_holidays_on_city_id", using: :btree
    t.index ["slug"], name: "index_holidays_on_slug", using: :btree
    t.index ["state_id"], name: "index_holidays_on_state_id", using: :btree
  end

  create_table "invoices", force: :cascade do |t|
    t.string   "subscription_id"
    t.string   "total"
    t.string   "description"
    t.string   "invoice_id"
    t.string   "status",          default: "aguardando"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["subscription_id"], name: "index_invoices_on_subscription_id", using: :btree
  end

  create_table "plan_services", force: :cascade do |t|
    t.integer  "plan_id"
    t.integer  "service_id"
    t.integer  "quantity"
    t.string   "slug"
    t.string   "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["plan_id"], name: "index_plan_services_on_plan_id", using: :btree
    t.index ["service_id"], name: "index_plan_services_on_service_id", using: :btree
  end

  create_table "plans", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.decimal  "price",       precision: 14, scale: 2, default: "0.0"
    t.string   "size",                                 default: "medium"
    t.integer  "complete",                             default: 1
    t.integer  "partial",                              default: 1
    t.boolean  "active",                               default: true
    t.string   "slug"
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.index ["slug"], name: "index_plans_on_slug", using: :btree
  end

  create_table "services", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.decimal  "price",       precision: 14, scale: 2, default: "0.0"
    t.boolean  "extra",                                default: false
    t.boolean  "active",                               default: true
    t.string   "slug"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.index ["slug"], name: "index_services_on_slug", using: :btree
  end

  create_table "states", force: :cascade do |t|
    t.string   "name"
    t.string   "uf",         limit: 2
    t.string   "country"
    t.boolean  "active",               default: true
    t.string   "slug"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["slug"], name: "index_states_on_slug", using: :btree
  end

  create_table "subscriptions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "company_id"
    t.integer  "plan_id"
    t.decimal  "total",      precision: 14, scale: 2, default: "0.0"
    t.string   "gateway_id"
    t.string   "status"
    t.string   "slug"
    t.string   "code"
    t.date     "init"
    t.date     "ends"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.index ["company_id"], name: "index_subscriptions_on_company_id", using: :btree
    t.index ["plan_id"], name: "index_subscriptions_on_plan_id", using: :btree
    t.index ["user_id"], name: "index_subscriptions_on_user_id", using: :btree
  end

  create_table "user_schedules", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "company_id"
    t.string   "services"
    t.string   "extras"
    t.datetime "date"
    t.datetime "expiration"
    t.integer  "state"
    t.datetime "finished_at"
    t.datetime "cancelled_at"
    t.string   "slug"
    t.string   "code"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["company_id"], name: "index_user_schedules_on_company_id", using: :btree
    t.index ["user_id"], name: "index_user_schedules_on_user_id", using: :btree
  end

  create_table "user_vehicles", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "brand_id"
    t.integer  "vehicle_id"
    t.string   "identifier"
    t.string   "year"
    t.string   "model"
    t.boolean  "active",     default: true
    t.string   "slug"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["brand_id"], name: "index_user_vehicles_on_brand_id", using: :btree
    t.index ["slug"], name: "index_user_vehicles_on_slug", using: :btree
    t.index ["user_id"], name: "index_user_vehicles_on_user_id", using: :btree
    t.index ["vehicle_id"], name: "index_user_vehicles_on_vehicle_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "name"
    t.string   "email",                  default: "", null: false
    t.string   "identifier"
    t.string   "registration"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "phone"
    t.string   "mobile"
    t.string   "slug"
    t.string   "authorization"
    t.string   "fid"
    t.string   "gateway_id"
    t.string   "payment_id"
    t.string   "device"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["authorization"], name: "index_users_on_authorization", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["fid"], name: "index_users_on_fid", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "vehicles", force: :cascade do |t|
    t.integer  "brand_id"
    t.string   "name"
    t.string   "model"
    t.string   "year"
    t.string   "size",       default: "medium"
    t.boolean  "active",     default: true
    t.string   "slug"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["brand_id"], name: "index_vehicles_on_brand_id", using: :btree
    t.index ["slug"], name: "index_vehicles_on_slug", using: :btree
  end

end
