Geocoder.configure(
  timeout: 3,
  lookup: :google,
  language: 'pt-BR',            # ISO-639 language code
  # use_https: false,           # use HTTPS for lookup requests? (if supported)
  # http_proxy: nil,            # HTTP proxy server (user:pass@host:port)
  # https_proxy: nil,           # HTTPS proxy server (user:pass@host:port)
  api_key: 'AIzaSyCbjOeA5RjpqHYhiciPoa8rFIlScTNNIvQ',
  # cache: nil,                 # cache object (must respond to #[], #[]=, and #keys)
  # cache_prefix: 'geocoder:',  # prefix (string) to use for all cache keys

  # Calculation options
  units: :km,
  cache: Rails.cache,
  distances: :spherical
)
