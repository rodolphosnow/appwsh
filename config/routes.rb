# encoding: utf-8
Rails.application.routes.draw do

  root to: 'public#index'

  post 'create_invoice', to: 'webhooks#create_invoice'
  post 'changed_invoice', to: 'webhooks#changed_invoice'
  post 'changed_subscription', to: 'webhooks#changed_subscription'
  post 'subscription_changed', to: 'webhooks#subscription_changed'
  post 'activate_subscription', to: 'webhooks#activate_subscription'
  post 'expired_subscription', to: 'webhooks#expired_subscription'
  post 'suspended_subscription', to: 'webhooks#suspended_subscription'
  post 'subscription_renewed', to: 'webhooks#subscription_renewed'
  get 'password-reset', to: 'application#password_reset', as: 'password_reset'


  devise_for :users
  devise_for :admins, path: 'admin', path_names: {sign_in: 'login',   sign_out: 'logout', password: 'senha'}
  devise_for :companies, path: 'empresa', path_names: {sign_in: 'login',   sign_out: 'logout', password: 'senha'}

  get '/api', to: 'api#index'
  namespace :api do
    namespace :v1 do
      resources :users, only: [:create] do
        collection do
          get  'me'
          post 'auth'
          post 'device'
          post 'facebook_auth'
          put  'update'
          put  'update_image', path: 'update-image'
          post 'change_payment_method'
          delete 'logout'
          put  'password', path: 'update-password'
          post 'recover',  path: 'recover'
        end
      end

      scope 'user' do
        resources :vehicles,  except: [:new, :edit]
        resources :subscriptions
        resources :schedules, only: [:update, :show, :index, :destroy]
        resources :addresses, except: [:new, :edit] do
          collection do
              post 'zipcode'
          end
        end
      end

      resources :plans, only: [:index, :show] do
        get 'avaiables', on: :collection
      end

      resources :brands, only: [:index, :show]

      resources :companies, only: [:show] do
        resources :company_ratings, path: 'ratings', only: [:index, :create]
        resources :schedules, only: [:new, :create]
        collection do
          get 'near'
          get 'services'
          get 'search'
        end

        member do
          get 'calendar'
        end
      end
    end
  end

  #company
  get 'empresa', to: 'company#index', as: 'company'
  namespace :company , path: 'empresa' do
    resources :schedules, path: 'agendamentos', path_names: {new: 'novo', create: 'criar', update: 'atualizar'} do
      collection do
        get 'do-dia', as: 'todays', action: 'todays'
        get 'search_by_status', as: 'status', action: 'search_by_status'
        get 'search_by_date', as: 'date', action: 'search_by_date'
      end
      post 'finalizado', as: 'finished', action: 'finished', on: :member
      post 'abandonado', as: 'abandoned', action: 'abandoned', on: :member
    end

    resources :companies, path: 'dados-da-empresa', only: [:edit, :update]
  end

  #admin
  get 'admin', to: 'admin#index', as: 'admin'
  namespace :admin do

    resources :availabilities, path: 'disponibilidades', path_names: {new: 'novo', create: 'criar', update: 'atualizar'} do
      collection do
        get 'buscar', as: 'search', action: 'search'
      end
    end

    resources :brands, path: 'marcas', path_names: {new: 'novo', create: 'criar', update: 'atualizar'} do
      collection do
        get 'buscar', as: 'search', action: 'search'
      end
    end

    resources :companies, path: 'empresas', path_names: {new: 'novo', create: 'criar', update: 'atualizar'} do
      collection do
        get 'buscar', as: 'search', action: 'search'
      end
    end

    resources :holidays, path: 'feriados', path_names: {new: 'novo', create: 'criar', update: 'atualizar'} do
      collection do
        get 'buscar-por-data', as: 'search_by_date', action: 'search_by_date'
        get 'buscar-por-cidade', as: 'search_by_city', action: 'search_by_city'
        get 'buscar-por-estado', as: 'search_by_state', action: 'search_by_state'
        get 'cities_by_state/:id', action: 'cities_by_state'
      end
    end

    resources :invoices, path: 'pagamentos', path_names: {new: 'novo', create: 'criar', update: 'atualizar'} do
      collection do
        get 'buscar-por-data', as: 'search_by_date', action: 'search_by_date'
      end
    end

    resources :plans, path: 'planos', path_names: {new: 'novo', create: 'criar', update: 'atualizar'} do
      collection do
        get 'buscar', as: 'search', action: 'search'
      end
    end

    resources :services, path: 'servicos', path_names: {new: 'novo', create: 'criar', update: 'atualizar'} do
      collection do
        get 'buscar', as: 'search', action: 'search'
      end
    end

    resources :subscriptions, path: 'subscrição', path_names: {new: 'novo', create: 'criar', update: 'atualizar'} do
      collection do
        get 'buscar-por-empresa', as: 'search_by_company', action: 'search_by_company'
        get 'buscar-por-usuario', as: 'search_by_user', action: 'search_by_user'
        get 'buscar-por-plano', as: 'search_by_plan', action: 'search_by_plan'
      end
    end

    resources :users, path: 'usuarios', path_names: {new: 'novo', create: 'criar', update: 'atualizar'} do
      collection do
        get 'buscar', as: 'search', action: 'search'
      end
    end

    resources :user_schedules, path: 'agendamentos', path_names: {new: 'novo', create: 'criar', update: 'atualizar'} do
      collection do
        get 'buscar', as: 'search', action: 'search'
        get 'buscar-por-empresa', as: 'search_by_company', action: 'search_by_company'
        get 'buscar-por-usuario', as: 'search_by_user', action: 'search_by_user'
        get 'buscar-por-data', as: 'search_by_date', action: 'search_by_date'
      end
    end

  end



end
