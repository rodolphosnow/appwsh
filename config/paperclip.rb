Paperclip::Attachment.default_options[:url]          = ':s3_domain_url'
Paperclip::Attachment.default_options[:default_url]  = 'http://appwash.s3.amazonaws.com/'
Paperclip::Attachment.default_options[:s3_host_name] = 's3.amazonaws.com'

Paperclip.interpolates :code do |attachment, style|
  attachment.instance.code
end
