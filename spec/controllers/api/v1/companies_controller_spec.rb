require 'rails_helper'

RSpec.describe Api::V1::CompaniesController, type: :controller do

  subject(:user)    { create(:user) }
  subject(:company) { create(:company, :with_address) }

  before(:each) do
    request.headers.merge!({ 'Authorization': "Token token=#{ user.authorization }" })
  end

  describe 'GET #index' do
    before(:each) do
      request.headers.merge!({ 'Authorization': "Token token=#{ user.authorization }" })
    end

    context 'when company has address' do
      it 'should return companies json' do
        get :near, params: { marker: '-25.4771846,-49.2807934', radius: 5 }
        expect(response).to(have_http_status(:ok))
      end
    end

    context 'when company address is empty' do
      it 'should return empty array' do
        get :near, params: { marker: '', radius: 1 }
        json = JSON.parse(response.body)
        expect(json).to(be_empty)
      end
    end
  end

  describe 'GET #show' do
    context 'when company founded' do
      it 'should return company json' do
        get :show, params: { id: company.id }
        expect(response).to(have_http_status(:ok))
      end
    end

    context 'when company not found' do
      it 'should return not found' do
        get :show, params: { id: Time.now.to_i }
        expect(response).to(have_http_status(:not_found))
      end
    end
  end

  describe 'GET #services' do
    before do
      company.services << [ create(:service), create(:service, :extra) ]
    end

    context 'when company founded' do
      it 'should return company services json' do
        get :services, params: { id: company.id }
        expect(response).to(have_http_status(:ok))
        expect(JSON.parse(response.body)).to_not(be_empty)
      end
    end

    context 'when company not found' do
      it 'should return not found' do
        get :show, params: { id: Time.now.to_i }
        expect(response).to(have_http_status(:not_found))
      end
    end
  end

  describe 'GET #calendar' do
    before do
      company.services << [ create(:service), create(:service, :extra) ]
    end

    context 'when company founded' do
      it 'should return company calendar json' do
        get :services, params: { id: company.id }
        expect(response).to(have_http_status(:ok))
        expect(JSON.parse(response.body)).to_not(be_empty)
      end
    end

    context 'when company not found' do
      it 'should return not found' do
        get :show, params: { id: Time.now.to_i }
        expect(response).to(have_http_status(:not_found))
      end
    end
  end

end
