require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do

  describe 'strong parameters' do
    subject { build(:user) }
    it 'should permit user params on create' do
      attrs = { user: attributes_for(:user) }
      should permit(:fid, :name, :email, :password, :identifier, :registration, :phone, :mobile).for(:create, params: { params: attrs }).on(:user)
    end
  end

  describe 'POST #create' do
    subject { build(:user) }
    context 'with valid attributes' do
      it 'should return json with user data' do
        post :create, { params: { user: attributes_for(:user) }}
        expect(response).to(have_http_status(:ok))
        json = JSON.parse(response.body)
        expect(json['authorization']).to_not(be_empty)
      end
    end

    context 'with invalid attributes' do
      it 'should return unprocessable entity' do
        post :create, { params: { user: { name: 'Testing'}} }
        expect(response).to(have_http_status(:unprocessable_entity))
      end
    end
  end

  describe 'POST #auth' do
    subject { create(:user) }
    context 'with valid attributes' do
      it 'should return json with user data' do
        post :auth, { params: { user: { email: subject.email, password: '909090' }}}
        expect(response).to(have_http_status(:ok))
        json = JSON.parse(response.body)
        expect(json['authorization']).to_not(be_empty)
      end
    end

    context 'with invalid attributes' do
      it 'should return unprocessable entity' do
        post :create, { params: { user: { email: 'invalid', password: '909090'}} }
        expect(response).to(have_http_status(:unprocessable_entity))
      end
    end
  end

  describe 'GET #me' do
    subject { create(:user) }

    context 'with valid authorization' do
      it 'should return json with user data' do
        request.headers.merge!({ 'Authorization': "Token token=#{ subject.authorization }" })
        get :me, params: {}
        expect(response).to(have_http_status(:ok))
        json = JSON.parse(response.body)
        expect(json['authorization']).to_not(be_empty)
      end
    end

    context 'with invalid authorization' do
      it 'should return unauthorized' do
        request.headers.merge!({ 'Authorization': "Token token=#{ SecureRandom.hex(32).upcase }" })
        get :me, params: {}
        expect(response).to(have_http_status(:unauthorized))
      end
    end
  end

  describe 'PUT #update' do
    subject { create(:user) }

    before(:each) do
      request.headers.merge!({ 'Authorization': "Token token=#{ subject.authorization }" })
    end

    context 'with valid attributes' do
      it 'should return json with user data' do
        put :update, { params: { user: attributes_for(:user) }}
        expect(response).to(have_http_status(:ok))
        json = JSON.parse(response.body)
        expect(json['authorization']).to_not(be_empty)
      end
    end

    context 'with invalid attributes' do
      it 'should return unprocessable entity' do
        put :update, { params: { user: { name: 'x' }} }
        expect(response).to(have_http_status(:unprocessable_entity))
      end
    end
  end

end
