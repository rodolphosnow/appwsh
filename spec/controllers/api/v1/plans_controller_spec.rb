require 'rails_helper'

RSpec.describe Api::V1::PlansController, type: :controller do

  describe 'GET #index' do
    context 'when has active plans' do
      it 'should return plans json' do
        Plan.create(attributes_for(:plan))
        get :index
        expect(response).to(have_http_status(:ok))
      end
    end

    context 'when active plans are empty' do
      it 'should return not found' do
        get :index
        expect(response).to(have_http_status(:not_found))
      end
    end
  end

  describe 'GET #show' do
    subject { create(:plan) }

    context 'when plan founded' do
      it 'should return plan json' do
        get :show, params: { id: subject.id }
        expect(response).to(have_http_status(:ok))
      end
    end

    context 'when plan not found' do
      it 'should return not found' do
        get :show, params: { id: Time.now.to_i }
        expect(response).to(have_http_status(:not_found))
      end
    end
  end

end
