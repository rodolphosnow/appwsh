require 'rails_helper'

RSpec.describe Api::V1::AddressesController, type: :controller do

  subject { create(:user) }

  describe 'strong parameters' do
    before(:each) do
      request.headers.merge!({ 'Authorization': "Token token=#{ subject.authorization }" })
    end

    it 'should permit address params on create' do
      attrs = { address: attributes_for(:address) }
      should permit(:street, :number, :neighborhood, :city, :state, :zipcode).for(:create, params: { params: attrs }).on(:address)
    end
  end

  describe 'GET #index' do
    before(:each) do
      request.headers.merge!({ 'Authorization': "Token token=#{ subject.authorization }" })
    end

    context 'when user has address' do
      it 'should return vehicles json' do
        subject.add_address(attributes_for(:address))
        get :index
        expect(response).to(have_http_status(:ok))
      end
    end

    context 'when user address is empty' do
      it 'should return not found' do
        get :index
        expect(response).to(have_http_status(:not_found))
      end
    end
  end

  describe 'POST #create' do
    before(:each) do
      request.headers.merge!({ 'Authorization': "Token token=#{ subject.authorization }" })
    end

    context 'with valid attributes' do
      it 'should return address json' do
        post :create, { params: { address: attributes_for(:address) }}
        expect(response).to(have_http_status(:success))
      end
    end

    context 'with invalid attributes' do
      it 'should return unprocessable entity' do
        post :create, { params: { address: { street: 'Testing'}} }
        expect(response).to(have_http_status(:unprocessable_entity))
      end
    end
  end

  describe 'POST #update' do
    before(:each) do
      request.headers.merge!({ 'Authorization': "Token token=#{ subject.authorization }" })
    end

    context 'with valid attributes' do
      it 'should return address json' do
        subject.add_address(attributes_for(:address))
        put :update, { params: { address: attributes_for(:address), id: subject.address.id }}
        expect(response).to(have_http_status(:success))
      end
    end

    context 'with invalid attributes' do
      it 'should return unprocessable entity' do
        subject.add_address(attributes_for(:address))
        put :update, { params: { address: { street: 'x'}, id: subject.address.id } }
        expect(response).to(have_http_status(:unprocessable_entity))
      end
    end
  end

  describe 'DELETE #destroy' do
    before(:each) do
      subject.add_address(attributes_for(:address))
      request.headers.merge!({ 'Authorization': "Token token=#{ subject.authorization }" })
    end

    context 'when user address founded' do
      it 'should return success' do
        delete :destroy, params: { id: subject.address.id }
        expect(response).to(have_http_status(:success))
      end
    end

    context 'when user address not found' do
      it 'should return not found' do
        delete :destroy, params: { id: Time.now.to_i }
        expect(response).to(have_http_status(:not_found))
      end
    end
  end

  describe 'POST #zipcode' do
    before(:each) do
      request.headers.merge!({ 'Authorization': "Token token=#{ subject.authorization }" })
    end

    context 'with invalid zipcode' do
      it 'should return not found' do
        post :zipcode, params: { zipcode: "8001" }
        expect(response).to(have_http_status(:not_found))
      end
    end

    context 'with valid zipcode' do
      it 'should return success' do
        post :zipcode, params: { zipcode: "80010100" }
        expect(response).to(have_http_status(:success))
      end
    end
  end

end
