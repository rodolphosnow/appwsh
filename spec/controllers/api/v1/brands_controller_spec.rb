require 'rails_helper'

RSpec.describe Api::V1::BrandsController, type: :controller do

  describe 'GET #index' do
    context 'when has brands' do
      it 'should return brands json' do
        Brand.create(attributes_for(:brand))
        get :index
        expect(response).to(have_http_status(:ok))
      end
    end

    context 'when active plans are empty' do
      it 'should return not found' do
        get :index
        expect(response).to(have_http_status(:not_found))
      end
    end
  end

  describe 'GET #show' do
    subject { create(:user) }


    before(:each) do
      request.headers.merge!({ 'Authorization': "Token token=#{ subject.authorization }" })
    end
    context 'when brand founded' do
      it 'should return brand vehicles json' do
        brand = Brand.create(attributes_for(:brand))
        vehicle = create(:vehicle, brand_id: brand.id)
        get :show, params: { id: brand.id }
        expect(response).to(have_http_status(:ok))
      end
    end

    context 'when brand not found' do
      it 'should return not found' do
        get :show, params: { id: Time.now.to_i }
        expect(response).to(have_http_status(:not_found))
      end
    end
  end

end
