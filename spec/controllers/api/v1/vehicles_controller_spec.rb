require 'rails_helper'

RSpec.describe Api::V1::VehiclesController, type: :controller do

  subject { create(:user) }

  describe 'strong parameters' do
    before(:each) do
      request.headers.merge!({ 'Authorization': "Token token=#{ subject.authorization }" })
    end

    it 'should permit vehicle params on create' do
      attrs = { vehicle: attributes_for(:user_vehicle) }
      should permit(:brand_id, :vehicle_id, :identifier).for(:create, params: { params: attrs }).on(:vehicle)
    end
  end

  describe 'GET #index' do
    before(:each) do
      request.headers.merge!({ 'Authorization': "Token token=#{ subject.authorization }" })
    end

    context 'when user has vehicles' do
      it 'should return vehicles json' do
        subject.add_vehicle(attributes_for(:user_vehicle))
        get :index
        expect(response).to(have_http_status(:ok))
      end
    end

    context 'when user vehicles is empty' do
      it 'should return not found' do
        get :index
        expect(response).to(have_http_status(:not_found))
      end
    end
  end

  describe 'GET #show' do
    before(:each) do
      subject.add_vehicle(attributes_for(:user_vehicle))
      request.headers.merge!({ 'Authorization': "Token token=#{ subject.authorization }" })
    end

    context 'when user vehicle founded' do
      it 'should return vehicle json' do
        get :show, params: { id: subject.vehicle.id }
        expect(response).to(have_http_status(:success))
      end
    end

    context 'when user vehicle not found' do
      it 'should return not found' do
        subject.vehicle.destroy
        get :show, params: { id: Time.now.to_i }
        expect(response).to(have_http_status(:not_found))
      end
    end
  end

  describe 'POST #create' do
    before(:each) do
      request.headers.merge!({ 'Authorization': "Token token=#{ subject.authorization }" })
    end

    context 'with valid attributes' do
      it 'should return json with user data' do
        post :create, { params: { vehicle: attributes_for(:user_vehicle) }}
        expect(response).to(have_http_status(:success))
      end
    end

    context 'with invalid attributes' do
      it 'should return unprocessable entity' do
        post :create, { params: { vehicle: { name: 'Testing'}} }
        expect(response).to(have_http_status(:unprocessable_entity))
      end
    end
  end

  describe 'DELETE #destroy' do
    before(:each) do
      subject.add_vehicle(attributes_for(:user_vehicle))
      request.headers.merge!({ 'Authorization': "Token token=#{ subject.authorization }" })
    end

    context 'when user vehicle founded' do
      it 'should return vehicle json' do
        delete :destroy, params: { id: subject.vehicle.id }
        expect(response).to(have_http_status(:success))
      end
    end

    context 'when user vehicle not found' do
      it 'should return not found' do
        subject.vehicle.destroy
        delete :destroy, params: { id: Time.now.to_i }
        expect(response).to(have_http_status(:not_found))
      end
    end
  end

end
