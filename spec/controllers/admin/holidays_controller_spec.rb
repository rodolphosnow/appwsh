require 'rails_helper'

RSpec.describe Admin::HolidaysController, type: :controller do
  let(:resource) { create(:admin) }
  sign_in_admin



  describe "GET #index" do
    it "assigns @holidays" do
      city = create(:city)
      holiday = create(:holiday)
      process :index, method: :get
      expect(assigns(:holidays)).to eq([holiday])
      expect(response).to(have_http_status(200))
      expect(response).to render_template(:index)
    end
  end

   describe "Get #new" do
    before(:each) do
       city = create(:city)
    end
    before { process :new, method: :get}
    it { expect(response).to(have_http_status(200)) }
    it { expect(assigns(:holiday)).to(be_a_new(Holiday)) }
    it { expect(response).to(render_template('admin/holidays/new')) }
  end


   describe 'holiday #create' do
    let(:attrs) { attributes_for(:holiday) }

    it { expect { process :create, method: :post }.to(raise_error(ActionController::ParameterMissing)) }

    context 'is invalid when params are present but wrong' do
      before { process :create, method: :post, params: { holiday: {date: ''} } }
      it { expect(response).to(render_template('admin/holidays/new')) }
    end

    context 'is valid when params are present and right' do
      before { process :create, method: :post, params: { holiday: attrs } }
      it { expect(response).to(redirect_to([:admin, :holidays])) }
    end
  end

  describe 'GET #edit' do
    let(:holiday) { create(:holiday) }
    before {  process :edit, method: :get, params: {id: holiday.id} }

    it { expect(response).to have_http_status(200) }
    it { expect(assigns(:holiday)).to(eq(holiday)) }
    it { expect(response).to(render_template('admin/holidays/edit')) }
  end

  describe 'PUT #update' do
    let(:record) { create(:holiday) }
    let(:attrs)  { attributes_for(:holiday) }

    it { expect { process :update, method: :put, params: { id: record.id } }.to(raise_error(ActionController::ParameterMissing)) }

    context 'is invalid when params are present but wrong' do
      before { process :update, method: :put, params: { id: record.id, holiday: {date: ''} } }
      it { expect(response).to(render_template('admin/holidays/edit')) }
    end

    context 'is valid when params are present and right' do
      before { process :update, method: :put, params: { id: record.id, holiday: attrs } }
      it { expect(response).to(redirect_to([:admin, :holidays])) }
    end
  end

end
