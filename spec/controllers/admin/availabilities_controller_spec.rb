require 'rails_helper'

RSpec.describe Admin::AvailabilitiesController, type: :controller do
  let(:resource) { create(:admin) }
  sign_in_admin

  describe "GET #index" do
    it "assigns @availabilities" do
      availability = create(:availability)
      process :index, method: :get
      expect(assigns(:availabilities)).to eq([availability])
      expect(response).to(have_http_status(200))
      expect(response).to render_template(:index)
    end
  end

   describe "Get #new" do
    before { process :new, method: :get}
    it { expect(response).to(have_http_status(200)) }
    it { expect(assigns(:availability)).to(be_a_new(Availability)) }
    it { expect(response).to(render_template('admin/availabilities/new')) }
  end

   describe 'availability #create' do
    let(:attrs) { attributes_for(:availability) }

    it { expect { process :create, method: :post }.to(raise_error(ActionController::ParameterMissing)) }

    context 'is invalid when params are present but wrong' do
      before { process :create, method: :post, params: { availability: {week_day: ''} } }
      it { expect(response).to(render_template('admin/availabilities/new')) }
    end

    context 'is valid when params are present and right' do
      before { process :create, method: :post, params: { availability: attrs } }
      it { expect(response).to(redirect_to([:admin, :availabilities])) }
    end
  end

  describe 'GET #edit' do
    let(:availability) { create(:availability) }
    before {  process :edit, method: :get, params: {id: availability.id} }

    it { expect(response).to have_http_status(200) }
    it { expect(assigns(:availability)).to(eq(availability)) }
    it { expect(response).to(render_template('admin/availabilities/edit')) }
  end

  describe 'PUT #update' do
    let(:record) { create(:availability) }
    let(:attrs)  { attributes_for(:availability) }

    it { expect { process :update, method: :put, params: { id: record.id } }.to(raise_error(ActionController::ParameterMissing)) }

    context 'is invalid when params are present but wrong' do
      before { process :update, method: :put, params: { id: record.id, availability: {week_day: ''} } }
      it { expect(response).to(render_template('admin/availabilities/edit')) }
    end

    context 'is valid when params are present and right' do
      before { process :update, method: :put, params: { id: record.id, availability: attrs } }
      it { expect(response).to(redirect_to([:admin, :availabilities])) }
    end
  end

end
