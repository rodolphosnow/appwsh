require 'rails_helper'

RSpec.describe Admin::BrandsController, type: :controller do
  let(:resource) { create(:admin) }
  sign_in_admin

  describe "GET #index" do
    it "assigns @brands" do
      brand = create(:brand)
      process :index, method: :get
      expect(assigns(:brands)).to eq([brand])
      expect(response).to(have_http_status(200))
      expect(response).to render_template(:index)
    end
  end

   describe "Get #new" do
    before { process :new, method: :get}
    it { expect(response).to(have_http_status(200)) }
    it { expect(assigns(:brand)).to(be_a_new(Brand)) }
    it { expect(response).to(render_template('admin/brands/new')) }
  end


   describe 'brand #create' do
    let(:attrs) { attributes_for(:brand) }

    it { expect { process :create, method: :post }.to(raise_error(ActionController::ParameterMissing)) }

    context 'is invalid when params are present but wrong' do
      before { process :create, method: :post, params: { brand: {name: ''} } }
      it { expect(response).to(render_template('admin/brands/new')) }
    end

    context 'is valid when params are present and right' do
      before { process :create, method: :post, params: { brand: attrs } }
      it { expect(response).to(redirect_to([:admin, :brands])) }
    end
  end

  describe 'GET #edit' do
    let(:brand) { create(:brand) }
    before {  process :edit, method: :get, params: {id: brand.id} }

    it { expect(response).to have_http_status(200) }
    it { expect(assigns(:brand)).to(eq(brand)) }
    it { expect(response).to(render_template('admin/brands/edit')) }
  end

  describe 'PUT #update' do
    let(:record) { create(:brand) }
    let(:attrs)  { attributes_for(:brand) }

    it { expect { process :update, method: :put, params: { id: record.id } }.to(raise_error(ActionController::ParameterMissing)) }

    context 'is invalid when params are present but wrong' do
      before { process :update, method: :put, params: { id: record.id, brand: {name: ''} } }
      it { expect(response).to(render_template('admin/brands/edit')) }
    end

    context 'is valid when params are present and right' do
      before { process :update, method: :put, params: { id: record.id, brand: attrs } }
      it { expect(response).to(redirect_to([:admin, :brands])) }
    end
  end


end
