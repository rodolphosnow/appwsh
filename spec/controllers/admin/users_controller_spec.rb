require 'rails_helper'

RSpec.describe Admin::UsersController, type: :controller do
  let(:resource) { create(:admin) }
  sign_in_admin

  describe "GET #index" do
    it "assigns @users" do
      user = create(:user)
      process :index, method: :get
      expect(assigns(:users)).to eq([user])
      expect(response).to(have_http_status(200))
      expect(response).to render_template(:index)
    end
  end

   describe "Get #new" do
    before { process :new, method: :get}
    it { expect(response).to(have_http_status(200)) }
    it { expect(assigns(:user)).to(be_a_new(User)) }
    it { expect(response).to(render_template('admin/users/new')) }
  end


  describe 'GET #edit' do
    let(:user) { create(:user) }
    before {  process :edit, method: :get, params: {id: user.id} }

    it { expect(response).to have_http_status(200) }
    it { expect(assigns(:user)).to(eq(user)) }
    it { expect(response).to(render_template('admin/users/edit')) }
  end

  describe 'PUT #update' do
    let(:record) { create(:user) }
    let(:attrs)  { attributes_for(:user) }

    it { expect { process :update, method: :put, params: { id: record.id } }.to(raise_error(ActionController::ParameterMissing)) }

    context 'is invalid when params are present but wrong' do
      before { process :update, method: :put, params: { id: record.id, user: {name: ''} } }
      it { expect(response).to(render_template('admin/users/edit')) }
    end

    context 'is valid when params are present and right' do
      before { process :update, method: :put, params: { id: record.id, user: attrs } }
      it { expect(response).to(redirect_to([:admin, :users])) }
    end
  end

end
