require 'rails_helper'

RSpec.describe Admin::PlansController, type: :controller do
  let(:resource) { create(:admin) }
  sign_in_admin

  describe "GET #index" do
    it "assigns @plans" do
      plan = create(:plan)
      process :index, method: :get
      expect(assigns(:plans)).to eq([plan])
      expect(response).to(have_http_status(200))
      expect(response).to render_template(:index)
    end
  end

   describe "Get #new" do
    before { process :new, method: :get}
    it { expect(response).to(have_http_status(200)) }
    it { expect(assigns(:plan)).to(be_a_new(Plan)) }
    it { expect(response).to(render_template('admin/plans/new')) }
  end


   describe 'plan #create' do
    let(:attrs) { attributes_for(:plan) }

    it { expect { process :create, method: :post }.to(raise_error(ActionController::ParameterMissing)) }

    context 'is invalid when params are present but wrong' do
      before { process :create, method: :post, params: { plan: {name: ''} } }
      it { expect(response).to(render_template('admin/plans/new')) }
    end

    context 'is valid when params are present and right' do
      before { process :create, method: :post, params: { plan: attrs } }
      it { expect(response).to(redirect_to([:admin, :plans])) }
    end
  end

  describe 'GET #edit' do
    let(:plan) { create(:plan) }
    before {  process :edit, method: :get, params: {id: plan.id} }

    it { expect(response).to have_http_status(200) }
    it { expect(assigns(:plan)).to(eq(plan)) }
    it { expect(response).to(render_template('admin/plans/edit')) }
  end

  describe 'PUT #update' do
    let(:record) { create(:plan) }
    let(:attrs)  { attributes_for(:plan) }

    it { expect { process :update, method: :put, params: { id: record.id } }.to(raise_error(ActionController::ParameterMissing)) }

    context 'is invalid when params are present but wrong' do
      before { process :update, method: :put, params: { id: record.id, plan: {title: ''} } }
      it { expect(response).to(render_template('admin/plans/edit')) }
    end

    context 'is valid when params are present and right' do
      before { process :update, method: :put, params: { id: record.id, plan: attrs } }
      it { expect(response).to(redirect_to([:admin, :plans])) }
    end
  end

end
