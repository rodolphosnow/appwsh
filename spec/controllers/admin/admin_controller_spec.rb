require 'rails_helper'

RSpec.describe AdminController, type: :controller do
  let(:resource) { create(:admin) }
  sign_in_admin

  describe 'GET #index' do

    it 'returns http success' do
      get :index
      expect(response).to(have_http_status(:success))
      expect(response).to render_template(:index)
    end
  end

end
