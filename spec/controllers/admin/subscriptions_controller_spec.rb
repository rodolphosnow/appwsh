require 'rails_helper'

RSpec.describe Admin::SubscriptionsController, type: :controller do
  let(:resource) { create(:admin) }
  sign_in_admin

  describe "GET #index" do
    it "assigns @subscriptions" do
      subscription = create(:subscription)
      process :index, method: :get
      expect(assigns(:subscriptions)).to eq([subscription])
      expect(response).to(have_http_status(200))
      expect(response).to render_template(:index)
    end
  end

end
