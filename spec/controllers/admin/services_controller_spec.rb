require 'rails_helper'

RSpec.describe Admin::ServicesController, type: :controller do
  let(:resource) { create(:admin) }
  sign_in_admin

  describe "GET #index" do
    it "assigns @services" do
      service = create(:service)
      process :index, method: :get
      expect(assigns(:services)).to eq([service])
      expect(response).to(have_http_status(200))
      expect(response).to render_template(:index)
    end
  end

   describe "Get #new" do
    before { process :new, method: :get}
    it { expect(response).to(have_http_status(200)) }
    it { expect(assigns(:service)).to(be_a_new(Service)) }
    it { expect(response).to(render_template('admin/services/new')) }
  end


   describe 'service #create' do
    let(:attrs) { attributes_for(:service) }

    it { expect { process :create, method: :post }.to(raise_error(ActionController::ParameterMissing)) }

    context 'is invalid when params are present but wrong' do
      before { process :create, method: :post, params: { service: {name: ''} } }
      it { expect(response).to(render_template('admin/services/new')) }
    end

    context 'is valid when params are present and right' do
      before { process :create, method: :post, params: { service: attrs } }
      it { expect(response).to(redirect_to([:admin, :services])) }
    end
  end

  describe 'GET #edit' do
    let(:service) { create(:service) }
    before {  process :edit, method: :get, params: {id: service.id} }

    it { expect(response).to have_http_status(200) }
    it { expect(assigns(:service)).to(eq(service)) }
    it { expect(response).to(render_template('admin/services/edit')) }
  end

  describe 'PUT #update' do
    let(:record) { create(:service) }
    let(:attrs)  { attributes_for(:service) }

    it { expect { process :update, method: :put, params: { id: record.id } }.to(raise_error(ActionController::ParameterMissing)) }

    context 'is invalid when params are present but wrong' do
      before { process :update, method: :put, params: { id: record.id, service: {name: ''} } }
      it { expect(response).to(render_template('admin/services/edit')) }
    end

    context 'is valid when params are present and right' do
      before { process :update, method: :put, params: { id: record.id, service: attrs } }
      it { expect(response).to(redirect_to([:admin, :services])) }
    end
  end

end
