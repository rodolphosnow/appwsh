require 'rails_helper' 

RSpec.describe CompanyService, type: :model do

  describe 'associations' do
    it { should belong_to(:company) }
    it { should belong_to(:service) }
  end

end
