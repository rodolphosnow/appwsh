require 'rails_helper'

RSpec.describe State, type: :model do

  subject { build(:state) }

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_least(3).is_at_most(64) }
    it { should_not(allow_value('!^&%@#*()? 0123456789.').for(:name)) }

    it { should validate_length_of(:uf).is_equal_to(2) }
    it { should_not(allow_value('!^&%@#*()? 0123456789.').for(:uf)) }

    it { expect(subject).to(be_valid) }
  end

  describe 'associations' do
    it { should(have_many(:cities)) }
  end

  subject { create(:state) }

  describe '#to_s' do
    it { expect(subject.to_s).to(eq(subject.name)) }
  end

  describe 'friendly_id must generate slug' do
    it { expect(subject.slug).to(eq(subject.name.parameterize)) }
  end

  describe '.find must use friendly_id (slug)' do
    it { expect(subject.class.send(:find, (subject.name.parameterize))).to(eq(subject)) }
  end

end
