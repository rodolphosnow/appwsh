require 'rails_helper'

RSpec.describe Company, type: :model do

  subject { build(:company) }

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_least(3).is_at_most(64) }

    it { should validate_presence_of(:email) }
    it { should allow_value(subject.email).for(:email) }
    it { should_not allow_value('foo').for(:email) }
    it { should_not(allow_value('!^&%@#*()? 0123456789.').for(:email)) }

    it { should validate_length_of(:password).is_at_least(6).is_at_most(32) }

    it { should validate_presence_of(:phone) }
    it { should_not(allow_value('!^&%@#*()? 0123456789.').for(:phone)) }

    it { should_not(allow_values('my phone', '(41) 3154', '(41) 31540-31540').for(:phone)) }
    it { should_not(allow_values(0, 12).for(:limit_per_hour)) }

    it { expect(subject).to(be_valid) }
  end

  describe 'associations' do
    it { should have_one(:address) }
    it { should have_many(:availabilities) }
    it { should have_many(:company_services) }
    it { should have_many(:services).through(:company_services) }
    it { should have_many(:schedules) }
    it { should accept_nested_attributes_for(:address) }
    it { should belong_to(:city) }
  end

  subject { create(:company, :with_address) }

  describe '#to_s' do
    it { expect(subject.to_s).to(eq(subject.name)) }
  end

  describe '#add_address' do
    it 'should add new company address' do
      expect(subject.add_address(attributes_for(:address))).to(be_truthy)
    end

    it 'should not add address with invalid params' do
      expect(subject.add_address({ street: 'x' })).to(be_falsy)
    end
  end

  describe '#to_json' do
    it { expect(subject).to(respond_to(:to_json)) }
  end

  describe '.near' do
    context 'with valid marker location' do
      it 'should return near companies' do
        expect(Company.near('-25.477185, -49.280793', 5)).to(include(subject))
      end
    end
  end

  describe 'after save must set city' do
    it { expect(subject.city).to_not(be_nil) }
  end

end
