require 'rails_helper'

RSpec.describe UserSchedule, type: :model do

  subject { build(:user_schedule, :tomorrow) }

  describe 'validations' do
    it { should validate_presence_of(:date) }
    it { should_not(allow_value('No date').for(:date)) }

    it 'not allow to schedule at same day' do
      resource = build(:user_schedule, :now)
      expect(resource).to_not(be_valid)
    end

    it 'not allow schedule before 08h' do
      resource = build(:user_schedule, :before_business_time)
      expect(resource).to_not(be_valid)
    end

    it 'not allow schedule after 17h' do
      resource = build(:user_schedule, :after_business_time)
      expect(resource).to_not(be_valid)
    end

    it { expect(subject).to(be_valid) }
  end

  describe 'associations' do
    it { should belong_to(:user) }
    it { should belong_to(:company) }
  end

  subject { build(:user_schedule, :tomorrow) }
  after(:each) { back_to_the_present }

  describe '#cancel!' do
    it 'not allow to cancel at less 2h before' do
      time_travel_to(subject.date - 1.hour)
      expect(subject.state).equal?(0)
    end

    it 'allow to cancel at less 2h before' do
      expect(subject.state).equal?(2)
    end
  end

  describe '#formatted_date' do
    it { expect(subject.formatted_date).to(eq(I18n.l(subject.date, format: :custom))) }
  end

end
