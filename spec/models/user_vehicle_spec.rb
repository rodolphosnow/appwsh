require 'rails_helper'

RSpec.describe UserVehicle, type: :model do

  subject { build(:user_vehicle) }

  describe 'validations' do
    it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:brand_id) }
    it { should validate_presence_of(:vehicle_id) }

    it { should validate_presence_of(:identifier) }
    it { should validate_length_of(:identifier).is_equal_to(8) }
    it { should validate_uniqueness_of(:identifier).case_insensitive }

    it { should_not(allow_values('ABCD-0000', '800-ABCD').for(:identifier)) }
    it { expect(subject).to(be_valid) }
  end

  describe 'associations' do
    it { should belong_to(:user)    }
    it { should belong_to(:brand)   }
    it { should belong_to(:vehicle) }
  end

  subject { create(:user_vehicle) }

  describe '#to_json' do
    it { expect(subject).to(respond_to(:to_json)) }
  end

end
