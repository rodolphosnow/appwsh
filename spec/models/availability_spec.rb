# require 'rails_helper'
#
# RSpec.describe Availability, type: :model do
#
#   subject { build(:availability) }
#
#   describe 'validations' do
#     it { should validate_presence_of(:week_day) }
#     it { should validate_length_of(:week_day).is_equal_to(3) }
#     it { should_not allow_values('AB', 'CDEF', '123').for(:week_day) }
#     it { should allow_value('SEG').for(:week_day) }
#
#     it { should validate_presence_of(:init) }
#     it { should validate_numericality_of(:init).is_greater_than_or_equal_to(6).is_less_than_or_equal_to(23) }
#
#     it { should validate_presence_of(:ends) }
#     it { should validate_numericality_of(:ends).is_greater_than_or_equal_to(6).is_less_than_or_equal_to(23) }
#
#     it { expect(subject).to(be_valid) }
#   end
#
#   subject { create(:availability) }
#
#   describe '#to_s' do
#     it { expect(subject.to_s).to(equal(subject.week_day)) }
#   end
#
# end
