# encoding: utf-8
require 'rails_helper'

RSpec.describe Admin, type: :model do

  subject { build(:admin) }

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_least(3).is_at_most(64) }

    it { should validate_presence_of(:email) }
    it { should allow_value(subject.email).for(:email) }
    it { should_not allow_value('foo').for(:email) }

    it { should validate_length_of(:password).is_at_least(6).is_at_most(32) }
    it { expect(subject).to(be_valid) }
  end

  subject { create(:admin) }

  describe '#to_s' do
    it { expect(subject.to_s).to(eq(subject.name)) }
  end

end
