require 'rails_helper'

RSpec.describe PlanService, type: :model do
  subject { build(:plan_service) }

  describe 'validations' do

    it { should validate_presence_of(:quantity) }
    it { should validate_numericality_of(:quantity) }

    it { should validate_presence_of(:plan) }

    it { expect(subject).to(be_valid) }
  end
end
