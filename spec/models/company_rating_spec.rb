require 'rails_helper'

RSpec.describe CompanyRating, type: :model do

  subject { build(:company_rating) }

  describe 'validations' do
    it { should validate_presence_of(:company) }
    it { should validate_length_of(:message).is_at_least(3).is_at_most(120) }
    it { should validate_numericality_of(:internal) }
    it { should validate_numericality_of(:external) }
    it { expect(subject).to(be_valid) }
  end

  describe 'associations' do
    it { should belong_to(:company) }
  end

  describe 'methods' do

  end

end
