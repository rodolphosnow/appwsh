require 'rails_helper'

RSpec.describe Vehicle, type: :model do

  describe 'validations' do
    let(:resource) { build(:vehicle) }

    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_least(2).is_at_most(64) }

    it { should validate_presence_of(:size) }
    it { should validate_inclusion_of(:size).in_array(['medium', 'large']) }

    it { should validate_presence_of(:brand_id) }
    it { expect(resource).to(be_valid) }
  end

  describe 'associations' do
    it { should belong_to(:brand) }
  end

  describe '#to_s' do
    let(:resource) { create(:vehicle) }
    context 'when vehicle implicit convertion to String' do
      it { expect(resource.to_s).to(equal(resource.name)) }
    end
  end

  describe '#to_json' do
    it { expect(subject).to(respond_to(:to_json)) }
  end

  describe '#fullname' do
    let (:fname) { [ subject.brand, subject.name, subject.model, subject.year ].compact.join(' ') }
    it { expect(subject.fullname).to(match(fname)) }
  end


end
