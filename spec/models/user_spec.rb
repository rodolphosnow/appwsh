require 'rails_helper'

RSpec.describe User, type: :model do

  subject { build(:user) }

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_least(3).is_at_most(64) }

    it { should validate_presence_of(:email) }
    it { should allow_value(subject.email).for(:email) }
    it { should_not allow_value('foo').for(:email) }

    it { should validate_length_of(:password).is_at_least(6).is_at_most(32) }
    it { expect(subject).to(be_valid) }
  end

  describe 'associations' do
    it { should have_one(:address) }
    it { should have_many(:schedules) }
    it { should have_one(:vehicle) }
    it { should accept_nested_attributes_for(:address) }
  end

  subject { create(:user) }

  describe '#to_s' do
    it { expect(subject.to_s).to(eq(subject.name)) }
  end

  describe '#to_json' do
    it { expect(subject).to(respond_to(:to_json)) }
  end

  describe '#authorization' do
    it { expect(subject.authorization).to_not(be_empty) }
  end

  describe '#add_vehicle' do

    let(:vehicle) { attributes_for(:user_vehicle) }
    it 'should add new user vehicle' do
      expect(subject.add_vehicle(vehicle)).to(be_truthy)
    end

    it 'should not add user vehicle with invalid params' do
      expect(subject.add_vehicle({ identifier: 'x' })).to(be_falsy)
    end
  end

  describe '#logout' do
    it { expect(subject.logout).to(be_nil) }
  end

  describe '#add_address' do
    let(:address) { attributes_for(:address) }
    it 'should add new user address' do
      expect(subject.add_address(address)).to(be_truthy)
    end

    it 'should not add user vehicle with invalid params' do
      expect(subject.add_address({ street: 'x' })).to(be_falsy)
    end
  end

end
