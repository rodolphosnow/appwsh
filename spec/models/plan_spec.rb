require 'rails_helper'

RSpec.describe Plan, type: :model do

  subject { build(:plan) }

  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_length_of(:title).is_at_least(5).is_at_most(64) }
    it { should_not(allow_value('!^&%@#*()? 0123456789.').for(:title)) }

    it { should validate_presence_of(:description) }
    it { should validate_length_of(:description).is_at_least(8).is_at_most(128) }

    it { should validate_presence_of(:price) }

    it { expect(subject).to(be_valid) }
  end

  subject { create(:plan) }

  describe '#to_s' do
    it { expect(subject.to_s).to(eq(subject.title)) }
  end

  describe '#to_json' do
    it { expect(subject).to(respond_to(:to_json)) }
  end

  describe '.active' do
    context 'should return only active plans' do
      it { expect(Plan.active).to include(subject) }
    end

    context 'should not return inactive plans' do
      let(:inactive) { create(:plan, { active: false }) }
      it { expect(PlanService.active).to_not include(inactive) }
    end
  end

end
