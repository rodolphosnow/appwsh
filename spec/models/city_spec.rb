require 'rails_helper'

RSpec.describe City, type: :model do

  subject { build(:city) }

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_least(3).is_at_most(64) }
    it { should_not(allow_value('!^&%@#*()? 0123456789.').for(:name)) }
    it { expect(subject).to(be_valid) }
  end

  describe 'associations' do
    it { should(belong_to(:state)) }
  end

  subject { create(:city) }

  describe '#to_s' do
    it { expect(subject.to_s).to(match(subject.location)) }
  end

  describe '#location' do
    let (:location) { [subject.name, subject.uf].join(', ') }
    it { expect(subject.to_s).to(match(location)) }
  end

  describe '#uf' do
    it { expect(subject.uf).to(eq(subject.state.uf)) }
  end

  describe 'friendly_id must generate slug' do
    it { expect(subject.slug).to(eq(subject.name.parameterize)) }
  end

  describe '.find must use friendly_id (slug)' do
    it { expect(subject.class.send(:find, (subject.name.parameterize))).to(eq(subject)) }
  end

end
