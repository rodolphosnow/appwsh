require 'rails_helper'

RSpec.describe Address, type: :model do

  subject { build(:address) }

  describe 'validations' do
    it { should validate_presence_of(:street) }
    it { should validate_length_of(:street).is_at_least(4).is_at_most(64) }

    it { should validate_presence_of(:number) }

    it { should validate_presence_of(:city) }
    it { should validate_length_of(:city).is_at_least(4).is_at_most(64) }

    it { should validate_presence_of(:state) }
    it { should validate_length_of(:state).is_at_least(2).is_at_most(64) }

    it { should validate_presence_of(:zipcode) }
    it { should validate_length_of(:zipcode).is_equal_to(9) }
    it { should_not allow_values('ABCDE-FGH', '81810-ABC').for(:zipcode) }

    it { expect(subject).to(be_valid) }
  end

  describe 'associations' do
    it { should(belong_to(:user))    }
    it { should(belong_to(:company)) }
  end

  subject { create(:address) }

  describe '#to_s' do
    it { expect(subject.to_s).to(match(subject.complete)) }
  end

  describe '#complete' do
    it { expect(subject.complete).to_not(be_empty) }
  end

  describe '#location' do
    it { expect(subject.location).to(match([subject.city, subject.state].join(', '))) }
  end

  describe '#to_json' do
    it { expect(subject).to(respond_to(:to_json)) }
  end

  describe '#marker' do
    it { expect(subject.marker).to(match([subject.latitude, subject.longitude])) }
  end

  describe '#geocoded_by' do
    it { expect(subject.latitude).to_not(be_nil)  }
    it { expect(subject.longitude).to_not(be_nil) }
  end

end
