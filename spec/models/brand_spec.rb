require 'rails_helper'

RSpec.describe Brand, type: :model do

  subject { build(:brand) }

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_least(3).is_at_most(64) }
    it { should_not(allow_value('!^&%@#*()? 0123456789.').for(:name)) }
    it { expect(subject).to(be_valid) }
  end

  describe 'associations' do
    it { should have_many(:vehicles) }
  end

  subject { create(:brand) }

  describe '#to_s' do
    it { expect(subject.to_s).to(eq(subject.name)) }
  end

  describe '#to_json' do
    it { expect(subject).to(respond_to(:to_json)) }
  end

end
