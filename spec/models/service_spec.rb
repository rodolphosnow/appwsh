require 'rails_helper'

RSpec.describe Service, type: :model do

  subject { build(:service) }

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_least(4).is_at_most(64) }

    it 'should be invalid if extra without price' do
      subject.extra = true; expect(subject).to_not(be_valid)
    end

    it { expect(subject).to(be_valid) }
  end

  subject { create(:service) }

  describe '#to_s' do
    it { expect(subject.to_s).to(eq(subject.name)) }
  end

  describe '#to_json' do
    it { expect(subject).to(respond_to(:to_json)) }
  end

end
