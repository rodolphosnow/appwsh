require 'rails_helper'

RSpec.describe Holiday, type: :model do

  subject { build(:holiday) }

  describe 'validations' do
    it { should validate_presence_of(:date) }
    it { should_not(allow_value('Date').for(:date)) }
    it { expect(subject).to(be_valid) }
  end

  describe 'associations' do
    it { should(belong_to(:city))  }
    it { should(belong_to(:state)) }
  end

  subject { create(:holiday) }

  describe '#to_s' do
    let(:idate) { I18n.l(subject.date, format: :default) }
    it { expect(subject.to_s).to(match(idate)) }
  end

end
