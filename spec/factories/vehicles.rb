FactoryGirl.define do
  factory :vehicle, class: 'Vehicle' do
    brand_id  1
    name      '307 HB Feline'
    model     'Hatch'
    year      '10/10'
    size      %w(medium large).sample
    active    true
  end
end
