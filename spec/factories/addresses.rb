FactoryGirl.define do
  factory :address, class: 'Address' do
    street 'Rua Roberto Koch'
    number '220'
    neighborhood 'Lindóia'
    complement 'Sobrado'
    city       'Curitiba'
    state      'PR'
    country    'Brasil'
    zipcode    '81010-220'
    active true
  end

  # factory :addr_jd_botanico, class: 'Address' do
  #   street 'R. Engenheiro Ostoja Roguski'
  #   number '690'
  #   neighborhood 'Jardim Botânico'
  #   complement ''
  #   city 'Curitiba'
  #   state 'PR'
  #   country 'Brasil'
  #   zipcode '80210-390'
  #   active true
  # end
  #
  # factory :addr_pq_barigui, class: 'Address' do
  #   street 'Av. Cândido Hartmann'
  #   number '2950'
  #   neighborhood 'Mercês'
  #   complement ''
  #   city 'Curitiba'
  #   state 'PR'
  #   country 'Brasil'
  #   zipcode '80710-570'
  #   active true
  # end
  #
  # factory :addr_sh_sao_jose, class: 'Address' do
  #   street 'Rua Dona Izabel A Redentora'
  #   number '1434'
  #   neighborhood 'Centro'
  #   complement ''
  #   city 'São José dos Pinhais'
  #   state 'PR'
  #   country 'Brasil'
  #   zipcode '83005-010'
  #   active true
  # end
end
