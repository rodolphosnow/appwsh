FactoryGirl.define do
  factory :state do
    name 'Paraná'
    uf   'PR'
    country 'Brasil'
    active true
  end
end
