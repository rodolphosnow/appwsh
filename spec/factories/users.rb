FactoryGirl.define do
  factory :user, class: 'User' do
    name       Faker::Name.name_with_middle
    phone      '(41) 3154-9570'
    email      Faker::Internet.free_email
    password   '909090'
  end
end
