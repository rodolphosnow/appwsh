FactoryGirl.define do
  factory :admin, class: 'Admin' do
    name       'Squarebits Software'
    email      'admin@squarebits.com.br'
    password   '@squarebits'
    password_confirmation '@squarebits'
  end
end
