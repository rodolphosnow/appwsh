FactoryGirl.define do
  factory :city do
    name 'Curitiba'
    state { build(:state, name: "Paraná") }
    active true
  end
end
