FactoryGirl.define do
  factory :holiday do
    date  Date.today
    state { build(:state, name: "Paraná") }
    city  { build(:city, name: "Curitiba")  }
    active true
  end
end
