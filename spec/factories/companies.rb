FactoryGirl.define do
  factory :company, class: 'Company' do
    sequence(:name) { |n| "Empresa Lava car #{n}" }
    identifier   '10.869.217/0796-04'
    registration '10869217079604'
    phone        '(41) 3342-3633'
    email        'batel@lavacar.com.br'
    password     'lavacar'
    limit_per_hour 1

    trait :with_address do
      association :address, factory: :address
      association :city, factory: :city
    end
  end
end
