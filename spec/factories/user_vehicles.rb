FactoryGirl.define do
  factory :user_vehicle, class: 'UserVehicle' do
    user_id     1
    brand_id    1
    vehicle_id  1
    identifier 'AAA-7031'
  end
end
