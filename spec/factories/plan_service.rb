FactoryGirl.define do
  factory :plan_service, class: 'PlanService' do
    quantity 2
    association :plan, factory: :plan
    association :service, factory: :service
  end
end
