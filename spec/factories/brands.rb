FactoryGirl.define do
  factory :brand, class: 'Brand' do
    name 'Peugeot'
    active true
  end
end
