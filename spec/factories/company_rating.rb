FactoryGirl.define do
  factory :company_rating, class: 'CompanyRating' do
    internal  5
    external  5
    message    'Bom atendimento'
    association :company, factory: :company
  end
end
