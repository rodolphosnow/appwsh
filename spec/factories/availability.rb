FactoryGirl.define do
  factory :availability, class: 'Availability' do
    week_day 'SEG'
    init 8
    ends 18
  end
end
