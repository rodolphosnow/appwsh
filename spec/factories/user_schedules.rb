FactoryGirl.define do
  factory :user_schedule, class: 'UserSchedule' do
    user_id    1
    company_id 1

    trait :now do
      date Time.zone.now
    end

    trait :tomorrow do
      date DateTime.parse("#{(Date.today + 1.day).to_s} 13:00:00")
    end

    trait :before_business_time do
      date (Time.zone.now.beginning_of_day)
    end

    trait :after_business_time do
      date (Time.zone.now.end_of_day)
    end

  end
end
