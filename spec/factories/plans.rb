FactoryGirl.define do
  factory :plan, class: 'Plan' do
    title 'CARWASH SMART'
    description 'Super lavagem completa +1 ducha.'
    price 39_90
    active true
  end
end
