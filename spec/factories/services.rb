FactoryGirl.define do
  factory :service, class: 'Service' do
    name 'Lavagem completa'
    description 'Lavagem completa para veiculos com aplicação de cera liquida'
    active true

    trait :inactive do
      active false
    end

    trait :extra do
      price 59.90
      extra true
    end
  end

end
