FactoryGirl.define do
  factory :subscription do
    association :user, factory: :user
    association :company, factory: :company
    association :plan, factory: :plan
    total 100.00
  end
end
