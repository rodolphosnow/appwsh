module ControllerMacros
  def sign_in_admin
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:admin]
      sign_in resource
    end
  end
end
