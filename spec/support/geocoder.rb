Geocoder.configure(lookup: :test)
Geocoder::Lookup::Test.set_default_stub([{ address: 'Rua Roberto Koch', state: 'Paraná', state_code: 'PR', country: 'Brazil', country_code: 'BR', latitude: -25.477185, longitude: -49.280793, coordinates: [-25.477185, -49.280793]}])
