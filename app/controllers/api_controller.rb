# encoding: utf-8
class ApiController < ActionController::Base
  include ActionController::HttpAuthentication::Token::ControllerMethods

  before_action :authenticate
  skip_before_action :authenticate, only: [:index]

  def index
    render(plain: 'SQUAREBITS SOFTWARE: WELCOME TO APP WASH API V1, 2017.')
  end

  protected

  def authenticate
    authenticate_token || render_unauthorized
  end

  def authenticate_token
    authenticate_with_http_token do |token, options|
      @user = User.find_by(authorization: token)
    end
  end

  def render_accepted
    headers['WWW-Authenticate'] = 'Token realm="Application"'
    render(json: { title: I18n.t('wsapi.titles.accepted'), message: I18n.t('wsapi.messages.accepted') }, status: :accepted)
  end

  def render_not_found
    headers['WWW-Authenticate'] = 'Token realm="Application"'
    render(json: { title: I18n.t('wsapi.titles.not_found'), message: I18n.t('wsapi.messages.not_found') }, status: :not_found)
  end

  def render_unauthorized
    headers['WWW-Authenticate'] = 'Token realm="Application"'
    render(json: { title: I18n.t('wsapi.titles.unauthorized'), message: I18n.t('wsapi.messages.unauthorized') }, status: :unauthorized)
  end

  def render_unprocessable(args)
    headers['WWW-Authenticate'] = 'Token realm="Application"'
    render(json: { title: I18n.t('wsapi.titles.unprocessable'), message: I18n.t('wsapi.messages.unprocessable', reason: args.errors.full_messages.to_sentence) }, status: :unprocessable_entity)
  end

  def render_unprocessable_without_object(msg)
    headers['WWW-Authenticate'] = 'Token realm="Application"'
    render(json: { title: I18n.t('wsapi.titles.unprocessable'), message: I18n.t('wsapi.messages.unprocessable', reason: msg) }, status: :unprocessable_entity)
  end

  def render_json(args)
    render(json: args, status: :ok)
  end

end
