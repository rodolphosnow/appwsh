class Company::SchedulesController < CompanyController

  def todays
    @company = current_company
    @schedules = @company.schedules.pending.todays.order(date: :desc).page(params[:page]).per(12)
    @partial = @schedules.present? ? 'todays' : 'none'
  end

  def index
    @company = current_company
    @schedules = @company.schedules.order(date: :desc).page(params[:page]).per(12)
    @partial = @schedules.present? ? 'schedules' : 'none'
  end

  def finished
    @schedule = UserSchedule.friendly.find(params[:id])
    @schedule.finished
    flash[:notice] = "Agendamento finalizado!"
    @user = @schedule.user
    redirect_to [:todays, :company, :schedules]
  end

  def abandoned
    @schedule = UserSchedule.friendly.find(params[:id])
    @schedule.abandoned
    flash[:notice] = "Agendamento finalizado!"
    @user = @schedule.user
    redirect_to [:todays, :company, :schedules]
  end

  def search_by_status
    status = search_params[:status]
    @company = current_company
    if status.eql?("Pendente")
      @schedules = @company.schedules.pending.page(params[:page]).per(12)
    else
      @schedules = @company.schedules.finished.page(params[:page]).per(12)
    end
    @partial = @schedules.present? ? 'search_result' : 'not_found'
    render 'search'
  end

  def search_by_date
    date_init = DateTime.parse(search_params[:date])
    date_end = DateTime.parse(search_params[:date]).end_of_day
    @company = current_company
    @schedules = @company.schedules.where('date >= ? AND date <= ?', date_init, date_end)
    @partial = @schedules.present? ? 'search_result' : 'not_found'
    render 'search'
  end


  private

  def search_params
    params.require(:search).permit(:date, :status)
  end

end
