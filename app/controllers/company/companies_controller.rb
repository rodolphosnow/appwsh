class Company::CompaniesController < CompanyController

  before_action :week_days, only: [:edit, :new]

  def edit
    @company = current_company
  end

  def update
    @company = current_company
    if @company.update_attributes(company_params)
      flash[:notice] = I18n.t('admin.messages.updated_f', model: Company.model_name.human)
      redirect_to [:company]
    else
      flash[:notice] = I18n.t('admin.messages.not_updated_f', model: Company.model_name.human)
      render 'edit'
    end
  end

  def week_days
    @week_days = Availability.active
    @seg = @week_days.seg
    @ter = @week_days.ter
    @qua = @week_days.qua
    @qui = @week_days.qui
    @sex = @week_days.sex
    @sab = @week_days.sab
    @dom = @week_days.dom
  end

  private

  def company_params
    params.require(:company).permit(:limit_per_hour, :name, :email, :identifier, :registration, :description, :phone, :mobile, :city_id, :active, address_attributes: [:id, :company_id, :street, :number, :neighborhood, :complement, :city, :state],availabilities_attributes: [:id, :week_day, :init, :ends, :_destroy], service_ids: [])
  end

  def search_params
    params.require(:search).permit(:date, :status)
  end

end
