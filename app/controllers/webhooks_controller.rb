class WebhooksController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def create_invoice
    puts params
    if Gateway.valid_token?(params[:data][:account_id])
      puts params
      if params[:data][:subscription_id].present?
        subscription = Subscription.find_by_gateway_id(params[:data][:subscription_id])
        invoice = Invoice.create(subscription_id: subscription.id, status: params[:data][:status], invoice_id: params[:data][:id], subscription_id: params[:data][:subscription_id])
      else
        invoice = Invoice.create(status: params[:data][:status], invoice_id: params[:data][:id])
      end
      puts invoice
      render nothing: true
    else
      render nothing: true
    end
  end

  def changed_invoice
    puts params
    if Gateway.valid_token?(params[:data][:account_id])
      invoice = Invoice.where(invoice_id: params[:data][:id]).first
      invoice.status = params[:data][:status]
      invoice.save
      puts invoice
      render nothing: true
    else
      render nothing: true
    end
  end

  def subscription_changed
    puts params
    if Gateway.valid_token?(params[:data][:account_id])
      subscription = Subscription.find_by_gateway_id(params[:data][:id])
      subscription.ends = params[:data][:expires_at]
      render nothing: true
    else
      render nothing: true
    end
  end

  def activate_subscription
    puts params
    if Gateway.valid_token?(params[:data][:account_id])
      subscription = Subscription.find_by_gateway_id(params[:data][:id])
      subscription.status = "1"
      subscription.save
      render nothing: true
    else
      render nothing: true
    end
  end

  def expired_subscription
    puts params
    if Gateway.valid_token?(params[:data][:account_id])
      subscription = Subscription.find_by_gateway_id(params[:data][:id])
      subscription.status = "3"
      subscription.ends = params[:data][:expires_at]
      subscription.save
      render nothing: true
    else
      render nothing: true
    end
  end

  def suspended_subscription
    puts params
    if Gateway.valid_token?(params[:data][:account_id])
      subscription = Subscription.find_by_gateway_id(params[:data][:id])
      subscription.status = "2"
      subscription.ends = Time.zone.now.to_date
      subscription.save
      render nothing: true
    else
      render nothing: true
    end
  end

  def subscription_renewed
    puts params
    if Gateway.valid_token?(params[:data][:account_id])
      subscription = Subscription.find_by_gateway_id(params[:data][:id])
      subscription.status = "1"
      subscription.ends = Time.zone.now.to_date + 30.days
      subscription.save
      render nothing: true
    else
      render nothing: true
    end
  end

end
