class Admin::SubscriptionsController < AdminController

  def index
    @subscriptions = Subscription.all.order(created_at: :desc).page(params[:page]).per(10)
    @partial = @subscriptions.present? ? 'subscriptions' : 'none'
  end

  def search_by_plan
    criteria = "#{params[:search][:plan]}"
    result = Subscription.where('plan_id = ?', criteria)
    set_search_result(result)
  end

  def search_by_company
    criteria = "#{params[:search][:company]}"
    result = Subscription.where('company_id = ?', criteria)
    set_search_result(result)
  end

  def search_by_user
    criteria = "%#{params[:search][:user]}%"
    users = User.where('name iLike ? or email iLike ?', criteria, criteria)
    result = Subscription.where(user_id: users.collect{|x| x.id})
    set_search_result(result)
  end

  def set_search_result(result)
    @count = result.length
    @partial = result.present? ? 'search_result' : 'not_found'
    @subscriptions = result.page(params[:page]).per(12)
    render 'search', format: :js
  end

end
