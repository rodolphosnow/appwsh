class Admin::ServicesController < AdminController

  before_action :service, only: [:show, :edit, :update]

  def index
    @services = Service.all.order(name: :asc).page(params[:page]).per(10)
    @partial = @services.present? ? 'services' : 'none'
  end

  def show

  end

  def new
    @service = Service.new
  end

  def edit

  end

  def create
    @service = Service.new(service_params)
    if @service.save
      flash[:notice] = I18n.t('admin.messages.created_m', model: Service.model_name.human)
      redirect_to [:admin, :services]
    else
      flash[:notice] =  I18n.t('admin.messages.not_created_m', model: Service.model_name.human)
      render 'new'
    end
  end

  def update
    if @service.update_attributes(service_params)
      flash[:notice] = I18n.t('admin.messages.updated_m', model: Service.model_name.human)
      redirect_to [:admin, :services]
    else
      flash[:notice] = I18n.t('admin.messages.not_updated_m', model: Service.model_name.human)
      render 'edit'
    end
  end

  def search
    criteria = "%#{params[:search][:term]}%"
    result = Service.where('title iLike ?', criteria)
    @count = result.length
    @partial = result.present? ? 'search_result' : 'not_found'
    @services = result.page(params[:page]).per(12)
    render 'search', format: :js
  end

  def service
    @service = Service.friendly.find(params[:id])
  end

  private

  def service_params
    params.require(:service).permit(:name, :description, :price, :extra, :active)
  end

end
