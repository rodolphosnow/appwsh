class Admin::InvoicesController < AdminController

  def index
    @invoices = Invoice.all.page(params[:page]).per(12)
    @partial = @invoices.present? ? 'invoices' : 'none'
  end

  def show
    @invoice = Invoice.find(params[:id])
  end

  def search_by_date
    date = DateTime.parse(search_params[:date])
    @invoices = Invoice.where('created_at >= ? AND created_at <= ?',
        date.beginning_of_day, date.end_of_day).page(params[:page])
    @partial = @invoices.present? ? 'search_result' : 'not_found'
  end

  private

  def search_params
    params.require(:search).permit(:date)
  end
end
