class Admin::HolidaysController < AdminController

  before_action :holiday, only: [:show, :edit, :update, :destroy]

  def index
    @holidays = Holiday.all.order(date: :desc).page(params[:page]).per(10)
    @partial = @holidays.present? ? 'holidays' : 'none'
  end

  def show

  end

  def new
    @holiday = Holiday.new
    @states = State.all.order(name: :asc)
    @selected = @states.where(name: "Paraná").first
    @cities = @selected.cities.order(name: :asc)
  end

  def edit
    @states = State.all.order(name: :asc)
    @selected = @states.where(name: "Paraná").first
    @cities = @selected.cities.order(name: :asc)
  end

  def create
    @holiday = Holiday.new(holiday_params)
    if @holiday.save
      flash[:notice] = I18n.t('admin.messages.created_m', model: Holiday.model_name.human)
      redirect_to [:admin, :holidays]
    else
      flash[:notice] =  I18n.t('admin.messages.not_created_m', model: Holiday.model_name.human)
      render 'new'
    end
  end

  def update
    if @holiday.update_attributes(holiday_params)
      flash[:notice] = I18n.t('admin.messages.updated_m', model: Holiday.model_name.human)
      redirect_to [:admin, :holidays]
    else
      flash[:notice] = I18n.t('admin.messages.not_updated_m', model: Holiday.model_name.human)
      render 'edit'
    end
  end

  def destroy
    @holiday.destroy
    flash[:notice] = I18n.t('admin.messages.destroyed_m', model: Holiday.model_name.human)
    redirect_to [:admin, :holidays]
  end

  def search_by_state
    criteria = "#{params[:search][:state]}"
    result = Holiday.where('state_id = ?', date)
    set_search_result(result)
  end

  def search_by_city
    criteria = "#{params[:search][:city]}"
    result = Holiday.where('city_id = ?', criteria)
    set_search_result(result)
  end

  def search_by_date
    criteria = "%#{params[:search][:date]}%"
    date = Date.parse(criteria)
    result = Holiday.where('date = ?', date)
    set_search_result(result)
  end

  def set_search_result(result)
    @count = result.length
    @partial = result.present? ? 'search_result' : 'not_found'
    @holidays = result.page(params[:page]).per(12)
    render 'search', format: :js
  end

  def holiday
    @holiday = Holiday.friendly.find(params[:id])
  end

  def cities_by_state
    @state = State.find(params[:id])
    @cities = @state.cities
    render partial: 'cities_select'
  end

  private

  def holiday_params
    params.require(:holiday).permit(:date, :state_id, :city_id, :active)
  end

end
