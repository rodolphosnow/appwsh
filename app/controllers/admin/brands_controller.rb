class Admin::BrandsController < AdminController

  before_action :brand, only: [:show, :edit, :update]

  def index
    @brands = Brand.all.order(name: :asc).page(params[:page]).per(10)
    @partial = @brands.present? ? 'brands' : 'none'
  end

  def show

  end

  def new
    @brand = Brand.new
  end

  def edit

  end

  def create
    @brand = Brand.new(brand_params)
    if @brand.save
      flash[:notice] = I18n.t('admin.messages.created_f', model: Brand.model_name.human)
      redirect_to [:admin, :brands]
    else
      flash[:notice] =  I18n.t('admin.messages.not_created_f', model: Brand.model_name.human)
      render 'new'
    end
  end

  def update
    if @brand.update_attributes(brand_params)
      flash[:notice] = I18n.t('admin.messages.updated_f', model: Brand.model_name.human)
      redirect_to [:admin, :brands]
    else
      flash[:notice] = I18n.t('admin.messages.not_updated_f', model: Brand.model_name.human)
      render 'edit'
    end
  end

  def search
    criteria = "%#{params[:search][:term]}%"
    result = Brand.where('name iLike ?', criteria)
    @count = result.length
    @partial = result.present? ? 'search_result' : 'not_found'
    @brands = result.page(params[:page]).per(12)
    render 'search', format: :js
  end

  def brand
    @brand = Brand.friendly.find(params[:id])
  end

  private

  def brand_params
    params.require(:brand).permit(:name, :code, :active, vehicles_attributes: [:id, :name, :model, :year, :size, :active, :_destroy])
  end


end
