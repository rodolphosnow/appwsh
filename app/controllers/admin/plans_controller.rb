class Admin::PlansController < AdminController
  before_action :plan, only: [:show, :edit, :update]

  def index
    @plans = Plan.all.order(created_at: :desc).page(params[:page]).per(10)
    @partial = @plans.present? ? 'plans' : 'none'
  end

  def show

  end

  def new
    @plan = Plan.new
  end

  def edit

  end

  def create
    @plan = Plan.new(plan_params)
    if @plan.save
      flash[:notice] = I18n.t('admin.messages.created_m', model: Plan.model_name.human)
      redirect_to [:admin, :plans]
    else
      flash[:notice] =  I18n.t('admin.messages.not_created_m', model: Plan.model_name.human)
      render 'new'
    end
  end

  def update
    if @plan.update_attributes(plan_params)
      flash[:notice] = I18n.t('admin.messages.updated_m', model: Plan.model_name.human)
      redirect_to [:admin, :plans]
    else
      flash[:notice] = I18n.t('admin.messages.not_updated_m', model: Plan.model_name.human)
      render 'edit'
    end
  end

  def search
    criteria = "%#{params[:search][:term]}%"
    result = Plan.where('title iLike ?', criteria)
    @count = result.length
    @partial = result.present? ? 'search_result' : 'not_found'
    @plans = result.page(params[:page]).per(12)
    render 'search', format: :js
  end

  def plan
    @plan = Plan.friendly.find(params[:id])
  end

  private

  def plan_params
    params.require(:plan).permit(:title, :description, :price_md, :price_lg, :active, plan_services_attributes: [:id, :service_id, :quantity, :_destroy])
  end
end
