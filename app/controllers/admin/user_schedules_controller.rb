class Admin::UserSchedulesController < AdminController

  before_action :user_schedule, only: [:show, :edit, :update]

  def index
    @user_schedules = UserSchedule.all.order(date: :desc).page(params[:page]).per(10)
    @partial = @user_schedules.present? ? 'user_schedules' : 'none'
  end

  def show

  end

  def new
    @user_schedule = UserSchedule.new
  end

  def edit

  end

  def create
    @user_schedule = UserSchedule.new(user_schedule_params)
    if @user_schedule.save
      flash[:notice] = I18n.t('admin.messages.created_m', model: UserSchedule.model_name.human)
      redirect_to [:admin, :user_schedules]
    else
      flash[:notice] =  I18n.t('admin.messages.not_created_m', model: UserSchedule.model_name.human)
      render 'new'
    end
  end

  def update
    if @user_schedule.update_attributes(user_schedule_params)
      flash[:notice] = I18n.t('admin.messages.updated_m', model: UserSchedule.model_name.human)
      redirect_to [:admin, :user_schedules]
    else
      flash[:notice] = I18n.t('admin.messages.not_updated_m', model: UserSchedule.model_name.human)
      render 'edit'
    end
  end

  def search_by_date
    criteria = "#{params[:search][:date]}"
    date = Date.parse(criteria)
    result = UserSchedule.where(' date >= ? AND date <= ? ', date.beginning_of_day, date.end_of_day)
    set_search_result(result)
  end

  def search_by_user
    criteria = "%#{params[:search][:user]}%"
    users = User.where('name iLike ? or email iLike ?', criteria, criteria)
    result = UserSchedule.where(user_id: users.collect{|x| x.id})
    set_search_result(result)
  end

  def search_by_company
    criteria = "#{params[:search][:company]}"
    result = UserSchedule.where(company_id: criteria)
    set_search_result(result)
  end

  def set_search_result(result)
    @count = result.length
    @partial = result.present? ? 'search_result' : 'not_found'
    @user_schedules = result.page(params[:page]).per(12)
    render 'search', format: :js
  end

  def user_schedule
    @user_schedule = UserSchedule.friendly.find(params[:id])
  end

  private

  def user_schedule_params
    params.require(:user_schedule).permit(:date, :expiration, :finished_at, :expiration, :cancelled_at, :company)
  end

end
