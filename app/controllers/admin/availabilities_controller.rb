class Admin::AvailabilitiesController < AdminController

  before_action :availability, only: [:show, :edit, :update]

  def index
    @availabilities = Availability.all.order(week_day: :asc).page(params[:page]).per(10)
    @partial = @availabilities.present? ? 'availabilities' : 'none'
  end

  def show

  end

  def new
    @availability = Availability.new
  end

  def edit

  end

  def create
    @availability = Availability.new(availability_params)
    if @availability.save
      flash[:notice] = I18n.t('admin.messages.created_m', model: Availability.model_name.human)
      redirect_to [:admin, :availabilities]
    else
      flash[:notice] =  I18n.t('admin.messages.not_created_m', model: Availability.model_name.human)
      render 'new'
    end
  end

  def update
    if @availability.update_attributes(availability_params)
      flash[:notice] = I18n.t('admin.messages.updated_m', model: Availability.model_name.human)
      redirect_to [:admin, :availabilities]
    else
      flash[:notice] = I18n.t('admin.messages.not_updated_m', model: Availability.model_name.human)
      render 'edit'
    end
  end

  def search
    criteria = "%#{params[:search][:week_day]}%"
    result = Availability.where('week_day iLike ?', criteria)
    @count = result.length
    @partial = result.present? ? 'search_result' : 'not_found'
    @availabilities = result.page(params[:page]).per(12)
    render 'search', format: :js
  end

  def availability
    @availability = Availability.friendly.find(params[:id])
  end

  private

  def availability_params
    params.require(:availability).permit(:week_day, :init, :ends, :active)
  end

end
