class Admin::CompaniesController < AdminController

  before_action :company, only: [:show, :edit, :update]
  before_action :week_days, only: [:edit, :new]

  def index
    @companies = Company.all.order(created_at: :desc).page(params[:page]).per(10)
    @partial = @companies.present? ? 'companies' : 'none'
  end

  def show

  end

  def new
    @company = Company.new
    @company.build_address
    @company.generate_availabilities
  end

  def edit

  end

  def week_days
    @week_days = Availability.active
    @seg = @week_days.seg
    @ter = @week_days.ter
    @qua = @week_days.qua
    @qui = @week_days.qui
    @sex = @week_days.sex
    @sab = @week_days.sab
    @dom = @week_days.dom
  end

  def create
    @company = Company.new(company_params)
    # raise @company.address.inspect
    if @company.save
      flash[:notice] = I18n.t('admin.messages.created_f', model: Company.model_name.human)
      redirect_to [:admin, :companies]
    else
      flash[:notice] =  I18n.t('admin.messages.not_created_f', model: Company.model_name.human)
      @company.build_address
      render 'new'
    end
  end

  def update
    if @company.update_attributes(company_params)
      flash[:notice] = I18n.t('admin.messages.updated_f', model: Company.model_name.human)
      redirect_to [:admin, :companies]
    else
      flash[:notice] = I18n.t('admin.messages.not_updated_f', model: Company.model_name.human)
      render 'edit'
    end
  end

  def search
    criteria = "%#{params[:search][:term]}%"
    result = Company.where('name iLike ?', criteria)
    @count = result.length
    @partial = result.present? ? 'search_result' : 'not_found'
    @companies = result.page(params[:page]).per(12)
    render 'search', format: :js
  end

  def company
    @company = Company.friendly.find(params[:id])
  end

  private

  def company_params
    params.require(:company).permit(:limit_per_hour, :password, :image, :name, :email, :identifier, :registration, :description, :phone, :mobile, :city_id, :active, address_attributes: [:id, :zipcode, :company_id, :street, :number, :neighborhood, :complement, :city, :state], availabilities_attributes: [:id, :week_day, :init, :ends, :_destroy], service_ids: [])
  end

  def address_params
    params.require(:company).permit(address_attributes: [:id, :company_id, :street, :number, :neighborhood, :complement, :city, :state, :zipcode])
  end

end
