class Admin::UsersController < AdminController

  before_action :user, only: [:show, :edit, :update]

  def index
    @users = User.all.order(created_at: :desc).page(params[:page]).per(10)
    @partial = @users.present? ? 'users' : 'none'
  end

  def show

  end

  def new
    @user = User.new
  end

  def edit

  end

  def update
    if @user.update_attributes(user_params)
      flash[:notice] = I18n.t('admin.messages.updated_m', model: User.model_name.human)
      redirect_to [:admin, :users]
    else
      flash[:notice] = I18n.t('admin.messages.not_updated_m', model: User.model_name.human)
      render 'edit'
    end
  end

  def search
    criteria = "%#{params[:search][:term]}%"
    result = User.where('name iLike ?', criteria)
    @count = result.length
    @partial = result.present? ? 'search_result' : 'not_found'
    @users = result.page(params[:page]).per(12)
    render 'search', format: :js
  end

  def user
    @user = User.friendly.find(params[:id])
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :identifier, :phone, :mobile)
  end


end
