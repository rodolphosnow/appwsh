class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception


  def after_sign_in_path_for(resource)
    if resource.class == Admin
      admin_path
    elsif resource.class == Company
      company_path
    else
      root_path
    end
  end

end
