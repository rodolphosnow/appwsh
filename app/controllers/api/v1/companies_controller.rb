class Api::V1::CompaniesController < ApiController

  before_action :authenticate, except: [:near, :search]

  def search
    criteria = "%#{params[:term]}%"
    @companies_by_name = Company.where('name iLike ?', criteria)
    @companies_by_address = Company.all.joins(:address).where('addresses.city iLike ? or addresses.state iLike ? or addresses.street iLike ? or addresses.neighborhood iLike ?', criteria, criteria, criteria, criteria)
    @companies = @companies_by_address + @companies_by_name
    if @companies.present?
      render_json( @companies.map(&:to_json) )
    else
      render_not_found
    end
  end

  def near
    @companies = Company.near(params[:marker], params[:radius])
    @companies = @companies.collect(&:to_json) if @companies.any?
    render_json(@companies)
  end

  def show
    @company = Company.find_by_id(id)
    @company ? render_json(@company.to_json) : render_not_found
  end

  def services
    @company = Company.find_by_id(id);
    if @company
      @services = @company.services
      @services.any? ? render_json(@services.collect(&:to_json)) : render_json([])
    else
      render_not_found
    end
  end

  def calendar
    @company = Company.find_by_id(id);
    @date    = params[:date].present? ? Date.parse(params[:date]) : Date.today
    if @company
      render_json(@company.calendar(@date))
    else
      render_not_found
    end
  end

  private

  def id
    params.require(:id)
  end

  def schedule_params
    params.require(:schedule).permit(:company_id, :date ,services: [])
  end

end
