class Api::V1::UsersController < ApiController

  before_action :authenticate
  skip_before_action :authenticate, only: [:create, :auth, :facebook_auth, :recover]

  def create
    @user = User.new(user_params)
    @user.save ? render_json(@user.to_json) : render_unprocessable(@user)
  end

  def auth
    @user = User.find_by_email(user_params[:email])
    if(@user && @user.valid_password?(user_params[:password]))
      @user = @user.keygen
      @user.update_tracked_fields!(request)
      render_json(@user.to_json)
    else
      render_not_found
    end
  end

  def recover
    @user = User.find_by_email(user_params[:email])
    if @user.present?
      @user.send_reset_password_instructions
      render_json(@user.to_json)
    else
      render_not_found
    end
  end

  def device
    if @user.update_attributes(device_params)
      render_json(@user.to_json)
    else
      render_unprocessable(@user)
    end
  end

  def password
    if @user.valid_password?(params[:current_password])
      if @user.update_attributes(password_params)
        render_json(@user.to_json)
      else
        render_unprocessable(@user)
      end
    else
      render_unprocessable(@user)
    end
  end

  def facebook_auth
    @user = User.find_by_fid(user_params[:fid])
    if @user
      @user.update_tracked_fields!(request)
      render_json(@user.to_json)
    else
      render_not_found
    end
  end

  def me
    @user ? render_json(@user.to_json) : render_unauthorized
  end

  def update
    if @user.update_attributes(user_params)
      render_json(@user.to_json)
    else
      render_unprocessable(@user)
    end
  end

  def update_image
    if @user.update_attributes(image: params[:image])
      render_json(@user.to_json)
    else
      render_unprocessable(@user)
    end
  end

  def change_payment_method
    token = params[:token]
    user = @user.change_payment_method(token)
    unless user.errors.present?
      render_accepted
    else
      render_unprocessable(user)
    end
  end

  def logout
    @user.logout
    render_accepted
  end

  private

  def image_params
    params.require(:image)
  end

  def user_params
    params.require(:user).permit(:image, :fid, :name, :email, :password, :password_confirmation, :identifier, :registration, :phone, :mobile)
  end

  def device_params
    params.require(:user).permit(:device)
  end

  def password_params
    params.require(:user).permit(:password, :password_confirmation)
  end

end
