class Api::V1::SubscriptionsController < ApiController

  before_action :authenticate

  def index
    subscription = @user.subscription
    if subscription
      render_json(subscription.to_json)
    else
      render_not_found
    end
  end

  def create
    token = params[:token]
    @user.generate_subscription(token, subscription_params)
    unless @user.errors.present?
      render_json(@user.subscription.to_json)
    else
      render_unprocessable(@user)
    end
  end

  def destroy
    subscription = @user.subscription
    response = subscription.cancel
    if response
      render_accepted
    else
      render_unprocessable
    end
  end


  private

  def subscription_params
    params.require(:subscription).permit(:company_id, :plan_id)
  end

end
