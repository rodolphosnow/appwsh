class Api::V1::SchedulesController < ApiController

  before_action :authenticate

  def index
    schedules = @user.schedules.order(date: :desc)
    if schedules
      render_json( schedules.map(&:to_json) )
    else
      render_not_found
    end
  end

  def new
    company = Company.find(params[:company_id])
    services = @user.prepare_schedule
    calendar = company.calendar
    services.merge!({ calendar: calendar.present? ? calendar : {} })
    render_json( services )
  end

  def create
    token = token_param
    schedule = @user.schedules.build(schedule_params)
    response = schedule.generate_schedule(token)
    if response.is_a?(UserSchedule) && !response.errors.present?
      render_json( schedule.to_json )
    elsif response.is_a?(UserSchedule) && response.errors.present?
      render_unprocessable(schedule)
    else
      render_unprocessable_without_object('Não foi possível efetuar a cobrança dos serviços extras')
    end
  end

  def show
    schedule = @user.schedules.find(params[:id])
    if schedule.present?
      render_json(schedule.to_json)
    else
      render_not_found
    end
  end

  def destroy
    schedule = @user.schedules.find(params[:id])
    if schedule.destroy
      render_json(schedule.to_json)
    else
      render_json(schedule)
    end
  end


  private

  def schedule_params
    params.require(:schedule).permit(:date, :company_id, services:[], extras:[])
  end

  def token_param
    params.require(:token)
  end

end
