class Api::V1::PlansController < ApiController

  skip_before_action :authenticate, except: [:avaiables]

  def index
    @plans = Plan.active
    if @plans.any?
      render_json(@plans.collect(&:to_json))
    else
      render_not_found
    end
  end

  def avaiables
    @plans = @user.present? ? Plan.where(size: @user.vehicle_size) : Plan.active
    if @plans.any?
      render_json(@plans.collect(&:to_json))
    else
      render_not_found
    end
  end

  def show
    @plan = Plan.active.find_by_id(id)
    @plan ? render_json(@plan.to_json) : render_not_found
  end

  private

  def id
    params.require(:id)
  end

end
