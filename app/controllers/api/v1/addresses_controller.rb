class Api::V1::AddressesController < ApiController

  before_action :authenticate, except: [:zipcode]

  def index
    @address = @user.address
    @address ? render_json(@address.to_json) : render_not_found
  end

  def create
    @address = @user.build_address(address_params)
    if @address.save
      render_json(@address.to_json)
    else
      render_unprocessable(@address)
    end
  end

  def update
    @address = Address.find_by_id(id)
    if @address && @address.update_attributes(address_params)
      render_json(@address.to_json)
    else
      render_unprocessable(@address)
    end
  end

  def destroy
    @address = Address.find_by_id(id)
    if @address && @address.destroy
      render_json({})
    else
      render_not_found
    end
  end

  def zipcode
    address = Address.search({zipcode: params_zipcode})
    address ? render_json(address) : render_not_found
  end

  private

  def address_params
    params.require(:address).permit(:street, :number, :neighborhood, :city, :state, :zipcode)
  end

  def id
    params.require(:id)
  end

  def params_zipcode
    params.require(:zipcode)
  end

end
