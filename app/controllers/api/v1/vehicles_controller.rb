class Api::V1::VehiclesController < ApiController

  before_action :authenticate

  def index
    if @user.vehicle.present?
      render_json(@user.vehicle.to_json)
    else
      render_not_found
    end
  end

  def show
    @vehicle = @user.vehicle
    @vehicle.present? ? render_json(@vehicle.to_json) : render_not_found
  end

  def create
    @vehicle = @user.build_vehicle(vehicle_params)
    if @vehicle.save
      render_json(@vehicle.to_json)
    else
      render_unprocessable(@vehicle)
    end
  end

  def destroy
    @vehicle = @user.vehicle
    if @vehicle && @vehicle.destroy
      render_json(@user.vehicle.to_json)
    else
      render_not_found
    end
  end

  private

  def vehicle_params
    params.require(:vehicle).permit(:brand_id, :vehicle_id, :identifier)
  end

  def id
    params.require(:id)
  end

end
