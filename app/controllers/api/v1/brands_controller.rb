class Api::V1::BrandsController < ApiController

  before_action :authenticate, except: [:index, :show]

  def index
    @brands = Brand.all
    @brands.present? ? render_json(@brands.map(&:to_json)) : render_not_found
  end

  def show
    @brand = Brand.find_by_id(id)
    return render_not_found unless @brand
    @brand_vehicles = @brand.vehicles.map(&:to_json)
    @brand_vehicles ? render_json(@brand_vehicles) : render_not_found
  end

  private

  def id
    params.require(:id)
  end

end
