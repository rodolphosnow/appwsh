class Api::V1::CompanyRatingsController < ApiController

  skip_before_action :authenticate, only: [:index, :create]

  def index
    @company = Company.includes(:ratings).find(params[:company_id])
    @ratings = @company.ratings
    @ratings.present? ? render_json( @ratings.map(&:to_json) ) : render_not_found
  end

  def create
    @rating = CompanyRating.new(rating_params)
    if @rating.save
      render_json(@rating.to_json)
    else
      raise @rating.errors.inspect
      render_unprocessable(@rating.to_json)
    end
  end

  private

  def rating_params
    params.require(:company_ratings).permit(:company_id, :internal, :external, :message)
  end

end
