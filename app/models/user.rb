# encoding: utf-8
class User < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  validates :name, presence: true, length: { in: (3..64) }
  validates :fid, uniqueness: true, allow_blank: true, allow_nil: true, case_sensitive: false

  has_one  :address, dependent: :destroy
  has_one :subscription, dependent: :destroy
  has_many :schedules, class_name: 'UserSchedule', dependent: :destroy
  has_one :vehicle,  class_name: 'UserVehicle', dependent: :destroy

  accepts_nested_attributes_for :address, reject_if: ->(f){ f[:street].blank? || f[:zipcode].blank? }
  before_create :keygen!
  after_create  :create_gateway_user
  before_validation :set_password_for_facebook_users, on: :create

  has_attached_file :image, default_url: "/images/image-default.jpg", path: '/users/images/:id.:extension'
  validates_attachment :image, content_type: { content_type: ['image/jpg', 'image/jpeg', 'image/png'] }

  def to_s
    name
  end

  def prepare_schedule
    services = avaiable_plan_services
    if services.present?
      extras = subscription_extra_services || {}
    else
      extras = {}
      services = {}
    end
    return {services: services.map(&:to_json), extras: extras.map(&:to_json)}
  end

  def subscription_extra_services
    return {} unless subscription
    subscription.company.services
  end

  def change_payment_method(token)
    begin
      self.remove_payment_method
    rescue

    end
    response = Gateway.create_payment_method(self, token)
    if response.respond_to?(:id)
      self.payment_id = response.id
      self.save
    else
      errors.add(:payment_id, "Não foi possível criar o método de pagamento, verifique os dados do cartão e tente novamente.")
    end
    self
  end

  def remove_payment_method
    response = Gateway.remove_payment_method(self)
    if response
      self.update_column(:payment_id, nil)
    end
  end

  def create_payment_method(token)
    response = Gateway.create_payment_method(self, token)
    if response.respond_to?(:id)
      self.payment_id = response.id
      self.save
      return true
    else
      errors.add(:payment_id, "Não foi possível criar o método de pagamento, verifique os dados do cartão e tente novamente.")
      return false
    end
  end

  def used_services
    used = Hash.new
    u.subscription.plan.plan_services.each{|x| used[x.service.id] = {"quantity" => x.quantity, "name" => x.service.name, "used" => 0} }
    used_services = u.actual_month_schedule.collect{|x| x.services}.compact
    used_services.each{|x| used[x[0].to_i]["used"] += 1}
    used
  end

  def services_usage
    used_services = used_services
    used = 0
    total = 0
    used_services.each{|k,v| used += v["used"]; total += v["quantity"]}
    return {total: total, used: used}
  end

  def set_subscription(params)
    plan = Plan.find(params[:plan_id])
    company = Company.find(params[:company_id])
    unless subscription.present?
      build_subscription
    end
    subscription.set_data(plan, company)
  end

  def generate_subscription(token, params)
    if self.payment_id.present?
      self.set_subscription(params)
    elsif token.present?
      if self.create_payment_method(token)
        self.set_subscription(params)
      end
    else
      self.errors.add(:payment_id, "Para criar uma assinatura é necessário possuir um método de pagamento, verifique os dados do cartão e tente novamente!")
    end
    self
  end

  def avaiable_plan_services
    return {} unless can_get_service
    plan_services = self.subscription.plan.plan_services.collect{|x| {"id" => x.service.id,"quantity" => x.quantity} }
    used_services = self.actual_month_schedule.collect{|x| x.services}.compact
    used_services_quantities = used_services.inject(Array.new) {|x, e| x.push(e[0]); x.push(e[1]) }.inject(Hash.new(0)) { |h, e| h[e] += 1; h}
    used_services_quantities.delete(nil)
    avaiables = plan_services.collect{|h| h["id"] if (used_services_quantities[h["id"].to_s] < h["quantity"])}.compact
    services = {}
    if avaiables.compact.uniq.present?
      services = Service.where(id: avaiables)
    end
    services
  end

  def actual_month_schedule
    ending_date = subscription.ends
    schedules.where('date > ?', (ending_date - 30.days) )
  end

  def can_get_service
    return false unless subscription.present?
    subscription.situation.eql?("Aprovado") ? true : false
  end

  def add_vehicle(args)
    build_vehicle(args).save
  end

  def add_address(args)
    build_address(args).save
  end

  def to_json
    { image: image.exists? ? image.url : "", name: name, email: email, phone: (phone || mobile), vehicle: vehicle.present? ? vehicle.to_json : {}, authorization: authorization, subscription: subscription.present? ? subscription.to_json : {}, schedules: schedules.present? ? schedules.map(&:to_json) : [], address: address.present? ? address.to_json : {} }
  end

  def vehicle_size
    if vehicle.present?
      vehicle.vehicle.size
    else
      "medium"
    end
  end

  def logout
    self.update_column(:authorization, nil)
    self.authorization
  end

  def keygen
    self.update_column(:authorization, SecureRandom.hex(32).upcase)
    return self
  end

  private

  def keygen!
    self.authorization = SecureRandom.hex(32).upcase
  end

  def set_password_for_facebook_users
    if self.fid.present?
      self.password = SecureRandom.hex(8)
    end
  end

  def create_gateway_user
    response = Gateway.create_customer(self)
    self.gateway_id = response.id
    self.save
  end

end
