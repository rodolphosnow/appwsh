class Invoice < ApplicationRecord
  belongs_to :subscription, foreign_key: "gateway_id"

  scope :paid, -> { where(status: "paid") }

  after_create :populate_invoice


  STATUSES = %w{pending paid canceled partially_paid refunded expired authorized in_protest chargeback}

  STATUS = {
    "pending"   => "pendente",
    "paid" => "pago",
    "canceled" => "cancelado",
    "partially_paid" => "parcialmente pago",
    "refunded" => "reembolsados",
    "expired" => "expirado",
    "authorized" => "autorizado"
  }

  private

  def populate_invoice
    response = Gateway.invoice(invoice_id)
    self.description = response.items.collect{|z| z["description"]}.to_sentence
    self.total = response.total
    self.save
  end

end
