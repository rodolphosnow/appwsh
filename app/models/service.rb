class Service < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  include ActionView::Helpers::NumberHelper

  validates :name,  presence: true, length: { in: (4..64) }
  validates :price, presence: true, if: ->(c){ c.extra? }
  validate :price_of_extra

  scope :active, -> { where(active: true) }
  scope :extra, -> { where(extra: true, active: true) }

  def to_s
    name
  end

  def price_of_extra
    if extra && price <= 1.0
      errors.add(:price, 'Serviço extra tem que ter o preço maior que R$ 1,00')
    end
  end

  def to_json
    extra? ? extra_attrs : attrs
  end

  private

  def attrs
    { id: id, name: name, description: description }
  end

  def extra_attrs
    attrs.merge!({ price: price.to_f, reais: number_to_currency(price), extra: extra })
  end

end
