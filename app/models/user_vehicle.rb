class UserVehicle < ApplicationRecord

  belongs_to :user
  belongs_to :brand
  belongs_to :vehicle

  validates :user_id, :vehicle_id, :brand_id, :identifier, presence: true
  validates_uniqueness_of :identifier, case_sensitive: false
  validates :identifier, length: { is: 8 }, format: { with: /^\w{3}-\d{4}$/, multiline: true }

  def to_json
    { id: id, identifier: identifier, model: (vehicle && vehicle.fullname), size: (vehicle && vehicle.size) }
  end

  def to_s
    [identifier, (vehicle && vehicle.fullname)].join("-")
  end

end
