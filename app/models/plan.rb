class Plan < ApplicationRecord

  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  include ActionView::Helpers::NumberHelper

  has_many :plan_services
  has_many :services, through: :plan_services

  accepts_nested_attributes_for :plan_services, reject_if: :all_blank, allow_destroy: true

  validates :title, presence: true, length: { in: (5..64) }, format: { with: /\A[\p{L}\s\p{M}]+\z/ }
  validates :description, presence: true, length: { in: (8..128) }
  validates :price, presence: true

  scope :active, ->{ where(active: true) }

  after_create :create_gateway_plan

  def to_s
    title
  end

  def to_json
    { id: id, title: title, description: description, price: number_to_currency(price), size: size }
  end

  def price_cents
    (price * 100).to_i
  end

  private

  def create_gateway_plan
    Gateway.create_plan(self)
  end

end
