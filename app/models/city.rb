class City < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  validates :name, presence: true, format: { with: /\A[\p{L}\s\p{M}]+\z/ }, length: { in: (3..64) }
  belongs_to :state

  def to_s
    location
  end

  def location
    [ name, state.uf ].join(', ')
  end

  def uf
    state.uf
  end

end
