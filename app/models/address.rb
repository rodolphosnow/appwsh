class Address < ApplicationRecord

  belongs_to :user
  belongs_to :company

  validates :street,  :number, :city, presence: true
  validates_length_of :street, :city, in: (4..64)
  validates :state,   presence: true, length: { in: (2..64) }
  validates :zipcode, presence: true, length: { is: 9 }, format: { with: /^\d{5}-\d{3}$/, multiline: true }
  validates :neighborhood, presence: true

  def to_s
    complete
  end

  def complete
    [ street, number, neighborhood, city ].compact.join(', ')
  end

  def location
    [city, state].join(', ')
  end

  def self.search(params)
    return nil unless params[:zipcode].present?
    begin
      addr = BuscaEndereco.cep(params[:zipcode])
      { street: [addr[:tipo_logradouro], addr[:logradouro]].compact.join(' '), district: addr[:bairro], city: addr[:cidade], state: addr[:uf], zipcode: addr[:cep], complete: [[addr[:tipo_logradouro], addr[:logradouro]].compact.join(' '), addr[:bairro], addr[:cidade], addr[:uf]].compact.join(', ') }
    rescue => e
      nil
    end
  end

  def marker
    return nil unless self.geocoded?
    [latitude, longitude].collect(&:to_f)
  end

  def to_json
    { id: id, address: complete, location: location, latitude: latitude.to_s, longitude: longitude.to_s }
  end

  geocoded_by :complete; after_validation :geocode

end
