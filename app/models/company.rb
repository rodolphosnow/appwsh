class Company < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  validates :name,  presence: true, length: { in: (3..64) }
  validates :limit_per_hour, inclusion: (1..10), presence: true
  validates :phone, presence: true, phone: true
  validates :email, presence: true

  has_one  :address, dependent: :destroy
  has_many :availabilities, class_name: 'CompanyAvailability', dependent: :destroy
  has_many :company_services, dependent: :destroy
  has_many :services, through: :company_services
  has_many :ratings, dependent: :destroy, class_name: 'CompanyRating'
  has_many :schedules, class_name: 'UserSchedule'
  belongs_to :city

  after_validation :define_city_by_address, if: ->{ address.present? }

  accepts_nested_attributes_for :address, reject_if: :all_blank, allow_destroy: true
  validates_associated :address
  accepts_nested_attributes_for :availabilities, reject_if: :all_blank, allow_destroy: true
  validates_associated :availabilities

  has_attached_file :image, default_url: "/images/image-default.jpg", path: '/companies/images/:id.:extension'
  validates_attachment :image, content_type: { content_type: ['image/jpg', 'image/jpeg', 'image/png'] }

  def to_s
    name
  end

  def add_address(args)
    build_address(args).save
  end

  def generate_availabilities
    week_days = Defaults.week_days
    week_days.each do |w|
      self.availabilities.build(week_day: w, init: 8, ends: 18)
    end
    self
  end

  def self.near(args, radius = nil)
    args ||= '-25.429715, -49.271995'; radius ||= 3
    includes(:address).references(:address).merge(Address.near(args, radius))
  end

  # TODO: Write test!
  def calendar(date = Date.today)
    ini_date = (date + 1.day)
    end_date = (date + 30.days)
    holidays = Holiday.where(date: (ini_date..end_date), city_id: city_id).collect(&:date)

    schedulation = schedules ? schedules.where(date: (ini_date..end_date)).group_by(&:formatted_date) : Array.new
    availability = availabilities.map {|a| { wday: a.week_day, init: a.init, ends: a.ends } }

    mount = (ini_date..end_date).map do |d|
      wday   = I18n.l(d, format: '%a').parameterize.upcase
      period = availability.detect {|x| x[:wday].eql?(wday) }
      hours  = Array.new

      next unless period;

      (period[:init]...period[:ends]).each do |h|
        time  = DateTime.new(d.year, d.month, d.day, h, 0, 0, DateTime.now.zone)
        ftime = I18n.l(time, format: :custom)
        busy  = true
        if(schedulation.keys.any? && schedulation[ftime])
          busy = false if (schedulation[ftime].size >= limit_per_hour)
        end
        hours.push({ time: I18n.l(time, format: '%H:%M'), isAvailable: busy, datetime: I18n.l(time, format: :custom) })
      end

      { wday: wday, day: I18n.l(d, format: '%d'), month: I18n.l(d, format: '%B').upcase.first(3), available: holidays.exclude?(d), hours: hours }
    end
    mount.compact
  end

  def rating
    return 3.0 unless ratings.any?
    ext = ratings.where('external > 0').average(:external) || 0
    int = ratings.where('internal > 0').average(:internal) || 0
    [ext, int].sum
  end

  def to_json
    { id: id, name: name, description: description, phone: phone, mobile: mobile, address: (address && address.to_json), rating: {count: company_ratings_count, rating: rating} }
  end

  private

  def define_city_by_address
    self.city = City.find_by_slug(address.city.parameterize)
  end

end
