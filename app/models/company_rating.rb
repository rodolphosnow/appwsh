class CompanyRating < ApplicationRecord
  belongs_to :company, counter_cache: true

  validates :company, presence: true
  validates :internal, :external, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 5 }
  validates :message, length: { in: (3..120) }, allow_blank: true, allow_nil: true


  def to_json
    {message: message.present? ? message : 'Não informado', internal: internal > 0 ? internal : 'Não informado', external: external > 0 ? external : 'Não informado'}
  end

end
