class Subscription < ApplicationRecord
  extend FriendlyId
  friendly_id :code, use: [:slugged, :finders]

  belongs_to :company
  belongs_to :user
  belongs_to :plan
  has_many   :invoices

  before_create :generate_code

  STATUSES = %w{ Pendente Aprovado Cancelado Expirada Inadimplente}

  def to_json
    {id: slug, company: company.to_json, plan: plan.to_json, situation: situation}
  end

  def situation
    if ( paid_this_month && status.eql?("1") && not_expired )
      return "Aprovado"
    else
      return translated_status
    end
  end

  def translated_status
    if status.present?
      STATUSES[status.to_i]
    else
      return "Expirado"
    end
  end

  def create_gateway_subscription(plan)
    response = Gateway.create_subscription(self.user, plan)
    if response.respond_to?(:id)
      self.gateway_id = response.id
      self.status = "1"
      self.ends = Time.zone.now.to_date + 30.days
      self.save
    else
      self.errors.add(gateway_id: "Não foi possível criar a assinatura, tente novamente mais tarde")
    end
  end

  def paid_this_month
    invoices.paid.last.present? ? invoices.paid.last.created_at > Date.today - 30.days : false
  end

  def not_expired
    ends.present? ? ends > Date.zone.today : false
  end

  def set_data(plan, company)
    self.company_id = company.id
    self.plan_id = plan.id
    self.total = plan.price
    self.status = "0"
    unless self.gateway_id.present?
      self.create_gateway_subscription(plan)
    end
    self.save
  end

  def cancel
    response = Gateway.remove_subscription(self)
    user = self.user
    if response
      self.destroy
      user.remove_payment_method
      return true
    else
      return false
    end
  end

  private

  def generate_code
    self.code = SecureRandom.hex(5).upcase
    self.slug = self.code
  end



end
