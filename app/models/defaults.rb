class Defaults

  def self.vehicle_sizes
    {"Médio" => "medium", "Grande" => "large"}
  end

  def self.week_days
    ["SEG", "TER", "QUA", "QUI", "SEX", "SAB", "DOM"]
  end

end
