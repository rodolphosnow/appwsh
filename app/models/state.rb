class State < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]
  
  validates :name, presence: true, format: { with: /\A[\p{L}\s\p{M}]+\z/ }
  validates_length_of :name, in: (3..64)
  validates_length_of :uf, is: 2

  has_many :cities

  def to_s
    name
  end

end
