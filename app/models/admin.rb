# encoding: utf-8
class Admin < ActiveRecord::Base
  devise :database_authenticatable, :trackable, :validatable

  validates :name, presence: true, length: { in: (3..64) }

  def to_s
    name
  end

end
