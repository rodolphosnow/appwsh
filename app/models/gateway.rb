class Gateway

  def self.create_customer(customer)
    Iugu.api_key = '7777184000aef8c199f06d7b88845ee9'
    response = Iugu::Customer.create({
      email: customer.email,
      name: customer.name,
    })
    response
  end

  # def self.charge(user, token, items)
  #   _items = items.map{|h| {price_cents: (h["price"] * 100).to_i, quantity: 1, description: h["description"] } }
  #   response = Iugu::Charge.create({
  #     token: token,
  #     email: "contato@clickservices.com.br",
  #     items: [ _items ]
  #   })
  #   response
  # end

  def self.create_subscription(customer, plan)
    Iugu.api_key = '7777184000aef8c199f06d7b88845ee9'
    Iugu::Subscription.create({
      plan_identifier: plan.id,
      customer_id: customer.gateway_id,
      expires_at:  Time.zone.now.to_date,
      only_on_charge_success: true
    })
  end

  def self.remove_subscription(subscription)
    Iugu.api_key = '7777184000aef8c199f06d7b88845ee9'
    response = Iugu::Subscription.fetch({id: subscription.gateway_id})
    deleted = response.delete
    return deleted
  end

  def self.charge_extras(items, user, token)
    Iugu.api_key = '7777184000aef8c199f06d7b88845ee9'
    _items = items.map{|h| {price_cents: (h["price"] * 100).to_i, quantity: 1, description: h["description"] } }
    response = Iugu::Charge.create({
      token: token,
      email: "contato@clickservices.com.br",
      items: [ _items ]
    })
    response
  end

  def self.create_payment_token
    Iugu.api_key = '7777184000aef8c199f06d7b88845ee9'
    response = Iugu::PaymentToken.create({
        acccount_id: "CB1138FE8D634A159BD9CFB2B7ABFF3F",
        method: "credit_card",
        test: true,
        data: {
          number: "4111111111111111",
          verification_value: "123",
          first_name: "JOAO",
          last_name: "SILVA",
          month: "12",
          year: "2022"
        }
    })
    response
  end

  def self.get_payment_method(customer)
    Iugu.api_key = '7777184000aef8c199f06d7b88845ee9'
    response = Iugu::PaymentMethod.fetch({
        customer_id: customer.gateway_id
    })
    response
  end

  def self.remove_payment_method(customer)
    Iugu.api_key = '7777184000aef8c199f06d7b88845ee9'
    payment_method = Iugu::PaymentMethod.fetch({
        customer_id: customer.gateway_id,
        id: customer.payment_id
    })
    response = payment_method.delete
    return response
  end

  def self.create_payment_method(customer, token)
    # token = Gateway.create_payment_token.id
    Iugu.api_key = '7777184000aef8c199f06d7b88845ee9'
    response = Iugu::PaymentMethod.create({
        customer_id: customer.gateway_id,
        description: "Meu Cartão de Crédito",
        token: token,
        set_as_default: true
    })
    response
  end

  def create_invoice(customer, items)

  end

  def self.create_plan(plan)
    Iugu.api_key = '7777184000aef8c199f06d7b88845ee9'
    response = Iugu::Plan.create({
        name: plan.title,
        identifier: plan.id.to_s,
        interval: 1,
        interval_type: "months",
        payable_with: "credit_card",
        prices:[
          value_cents: plan.price_cents,
          currency: "BRL"
        ]
    })
    response
  end

  def self.valid_token?(token)
    Iugu.api_key = '7777184000aef8c199f06d7b88845ee9'
    return false if token.blank?
    token.eql?(TOKEN)
  end

  def self.search_customer(customer)
    Iugu.api_key = '7777184000aef8c199f06d7b88845ee9'
    response = Iugu::Customer.fetch(customer.gateway_id)
  end

  def iugu_key
    Iugu.api_key = '7777184000aef8c199f06d7b88845ee9'
  end

  def self.update_customer(customer)
    Iugu.api_key = '7777184000aef8c199f06d7b88845ee9'
    customer = Iugu::Customer.fetch(customer.iugu_id)
    customer.name = customer.name
    customer.notes = customer.notes
    customer.save
  end

  def self.invoice(invoice_id)
    Iugu.api_key = '7777184000aef8c199f06d7b88845ee9'
    return Iugu::Invoice.search(id: invoice_id)
  end

  def self.payment_status(invoice_id)
    Iugu.api_key = '7777184000aef8c199f06d7b88845ee9'
    return false unless invoice_id
    api_key
    _invoice = invoice(invoice_id)
    response = _invoice.status ? _invoice.status : false
    return _invoice.status
  end

  def self.api_key
    Iugu.api_key = '7777184000aef8c199f06d7b88845ee9'
  end

  STATUS = {
    "pending"   => "waiting",
    "paid" => "confirmed",
    "canceled" => "cancelado",
    "partially_paid" => "parcialmente pago",
    "refunded" => "refunded",
    "expired" => "expired",
    "authorized" => "authorized"
  }

  TOKEN = "CB1138FE8D634A159BD9CFB2B7ABFF3F"
end
