class Holiday < ApplicationRecord

  extend FriendlyId
  friendly_id :code, use: [:slugged, :finders]

  before_create :generate_code

  validates :date, presence: true, timeliness: { type: :date }

  belongs_to :state
  belongs_to :city

  def to_s
    I18n.l(date, format: :default)
  end

  private

  def generate_code
    self.code = SecureRandom.hex(5).upcase
    self.slug = self.code
  end

end
