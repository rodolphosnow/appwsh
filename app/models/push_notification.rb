require 'fcm'
class PushNotification

  def self.notification(user, activity = {}, message = {}, id ={})
    device = user.device
    return false unless device.present?
    api_key = NOTIFICATION_KEY; user_name = ""; user_name = user.name if activity.eql?("RatingActivity")
    fcm = FCM.new(api_key)
    options = {data: {id: id, title: "AppWash", activity: activity, user_name: user_name, text: message, sound: "default", color: "#253350",
		icon: "logo"} }
    response = fcm.send([device], options)
  end

  def self.notification_key
    "AAAAXoAvQTs:APA91bHpjWZWeEzcYDayNZsaUKn15SynrcVYMw7rabX5VOvb6yncQGUC7zVc_FswSEwP_5IIY4Cw3rqKX0WFiH8ob1R2RiVJBkyEyYanrWGaXs09pson7bNcH0_QDTORdtrLZgAFxa7E"
  end

  NOTIFICATION_KEY = "AAAAXoAvQTs:APA91bHpjWZWeEzcYDayNZsaUKn15SynrcVYMw7rabX5VOvb6yncQGUC7zVc_FswSEwP_5IIY4Cw3rqKX0WFiH8ob1R2RiVJBkyEyYanrWGaXs09pson7bNcH0_QDTORdtrLZgAFxa7E"

end
