class UserSchedule < ApplicationRecord
  extend FriendlyId
  friendly_id :code, use: [:slugged, :finders]

  belongs_to :user
  belongs_to :company

  serialize :services, Array
  serialize :extras, Array

  validates :date, presence: true, format: /\/|\d+|:/, timeliness: { type: :datetime }
  validate  :validate_schedulation
  validate  :date_cannot_be_in_the_past, on: :create
  validate  :only_one_schedule_per_day, on: :create
  validates :services, presence: true

  before_create :generate_code

  scope :finished, -> { where.not('finished_at is NULL') }
  scope :pending, -> { where('cancelled_at is NULL and finished_at is NULL') }
  scope :todays, -> { where('date >= ? AND date <= ?', DateTime.now.beginning_of_day, DateTime.now.end_of_day) }


  def to_json
    {company: company.to_json, id: slug, date: I18n.l(date, format: :short), services: services_names }
  end

  def finished
    self.update_column(:finished_at, Time.zone.now)
    PushNotification.notification(self.user, "RatingActivity", "Olá, #{self.user.name} avalie o serviço prestado no App Wash!",self.company.id)
  end

  def abandoned
    self.update_column(:finished_at, Time.zone.now)
  end

  def cancel!
    return false if ((Time.zone.now + 2.hours) >= date)
    update_attributes(state: 2, cancelled_at: Time.zone.now)
  end

  def formatted_date
    I18n.l(date, format: '%d/%m/%Y %H:00')
  end

  def services_names
    if (services.present? || extras.present?)
     return Service.where(id: services + extras).collect{|x| x.name}
    else
      return []
    end
  end

  def generate_schedule(token)
    return false unless token
    if self.save
      charge_for_extra(token) ? self : false
    else
      self
    end
  end

  def charge_for_extra(token)
    if extras.present?
      return false unless token
      services = Service.where(id: extras).collect{|x| {"price"=> x.price, "description" => x.description} }
      response = Gateway.charge_extras(services, self.user, token)
      if response.errors.present?
        self.destroy
        false
      else
        true
      end
    else
      true
    end
  end

  private

  OFFICE_HOUR = { init: 8, ends: 17 }

  def validate_schedulation
    errors.add(:date, I18n.t('wsapi.messages.cannot_be_today')) if(is_today?)
    errors.add(:date, I18n.t('wsapi.messages.must_be_business_hour')) unless(in_office_time?)
  end

  def only_one_schedule_per_day
    schedule = UserSchedule.where('date < ? AND date > ? AND user_id = ?', date.end_of_day, self.date.beginning_of_day, user_id)
    if schedule.present?
      errors.add(:date, I18n.t('wsapi.messages.already_have_schedule') )
    end
  end

  def date_cannot_be_in_the_past
    if date < Date.today
      errors.add(:date, I18n.t('wsapi.messages.cannot_be_in_the_past'))
    end
  end

  def is_today?
    return true unless date.try(:to_date)
    Date.today.eql?(date.to_date)
  end

  def in_office_time?
    return false unless date.try(:to_datetime)
    date.hour.between?(OFFICE_HOUR[:init], OFFICE_HOUR[:ends])
  end

  def generate_code
    self.code = SecureRandom.hex(5).upcase
    self.slug = self.code
  end

end
