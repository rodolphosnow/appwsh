class PlanService < ApplicationRecord
  extend FriendlyId
  friendly_id :code, use: [:slugged, :finders]

  belongs_to :plan
  belongs_to :service

  validates :quantity, presence: true, numericality: true
  validates :plan, presence: true
  validates :service, presence: true

  before_create :generate_code

  scope :active, -> { where(active: true) }


  def to_s
    name
  end

  def to_json
    {name: name, quantity: quantity, plan: plan.name}
  end


  private

  def generate_code
    self.code = SecureRandom.hex(5).upcase
    self.slug = self.code
  end


end
