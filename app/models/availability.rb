# encoding: utf-8
class Availability < ApplicationRecord
  extend FriendlyId
  friendly_id :code, use: [:slugged, :finders]

  validates :week_day, presence: true, length: { is: 3 }, format: { with: /\A[\p{L}\s\p{M}]+\z/ }
  validates :init, :ends, presence: true, numericality: { greater_than_or_equal_to: 6, less_than_or_equal_to: 23 }
  validate :init_ends_range

  scope :active, -> { where(active: true) }
  scope :seg, -> { where(week_day: "SEG") }
  scope :ter, -> { where(week_day: "TER") }
  scope :qua, -> { where(week_day: "QUA") }
  scope :qui, -> { where(week_day: "QUI") }
  scope :sex, -> { where(week_day: "SEX") }
  scope :sab, -> { where(week_day: "SAB") }
  scope :dom, -> { where(week_day: "DOM") }

  before_create :generate_code

  def to_s
    week_day
  end

  def to_label
    "#{week_day} | #{init}h:00 - #{ends}h:00"
  end

  private

  def generate_code
    self.code = SecureRandom.hex(5).upcase
    self.slug = self.code
  end

  def init_ends_range
    if init >= ends
      errors.add(:init, "horário inícial não pode ser maior ou igual ao horário final")
    end
  end

end
