# encoding: utf-8
class Brand < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  validates :name, presence: true, length: { in: (3..64) }, format: { with: /\A[\p{L}\s\p{M}]+\z/ }
  has_many :vehicles

  accepts_nested_attributes_for :vehicles, reject_if: :all_blank, allow_destroy: true

  def to_s
    name
  end

  def to_json
    { id: id, name: name }
  end

end
