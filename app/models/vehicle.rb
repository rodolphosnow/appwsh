# encoding: utf-8
class Vehicle < ApplicationRecord
  belongs_to :brand

  validates :name, presence: true, length: { in: (2..64) }
  validates :size, :brand_id, presence: true
  validates_inclusion_of :size, in: %w( medium large ), message: 'Porte inválido!'

  def to_s
    name
  end

  def fullname
    [ brand, name, model, year ].compact.join(' ')
  end

  def to_json
    { id: id, brand: brand.name, name: fullname, size: size }
  end

end
