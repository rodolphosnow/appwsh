//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require masked-input
//= require masked-money
//= require charts.min
//= require bootstrap-file-field
//= require bootstrap-datepicker
//= require cocoon
//= require jquery.validate.min
//= require jquery.countdown
//= require wow.min
//= require_self

$(document).on('ready', function(){

  $.validator.addMethod(
    "date",
    function(value, element) {
         var check = false;
         var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
         if( re.test(value)){
              var adata = value.split('/');
              var gg = parseInt(adata[0],10);
              var mm = parseInt(adata[1],10);
              var aaaa = parseInt(adata[2],10);
              var xdata = new Date(aaaa,mm-1,gg);
              if ( ( xdata.getFullYear() == aaaa ) && ( xdata.getMonth () == mm - 1 ) && ( xdata.getDate() == gg ) )
                   check = true;
              else
                   check = false;
         } else
              check = false;
         return this.optional(element) || check;
    },
    "Insira uma data válida"
  );

  $('body').on('shown.bs.modal', function() { applyInputMasks() });

  $('body').on('hidden.bs.modal', '.modal', function(event) {
    $(this).removeData("bs.modal").find(".modal-body").empty();
  });

  $('.alert-dismissible').delay(3500).fadeOut(); applyInputMasks();

  $('.full-height').css({
    'height': $(window).height()
  });

  if($(window).width() >= 1025){
    $('.unity-portal .schooling').css('margin-top', -$('.schooling').height());

    $(window).scroll(function(e){
      if($(window).scrollTop() >= 60) {
        $('.portal .navbar-brand').show();
        $('.portal #main-menu .navbar-nav').css('margin-top', 25);
      } else {
        $('.portal .navbar-brand').hide();
        $('.portal #main-menu .navbar-nav').css('margin-top', 0);
      }
    });
  }

  $('.portal-main').css({
    'margin-top': $('.header').outerHeight() - 20
  });

  $("#box-link").css({
    'height': $("#box-log").height()
  });

  $(window).scroll(function(e){
    if($(window).scrollTop() >= 60) {
      $('.portal .header').css('border-width', 0);
      $('.portal .upper-header').slideUp();
      $('.portal .main-header').slideUp();
      $('.portal .nav-main').css('box-shadow', '0px 0px 60px 0px rgba(0, 0, 0, 0.07)');
      $('.unity-portal .navbar').addClass('white-nav').css('box-shadow', '0px 0px 60px 0px rgba(0, 0, 0, 0.07)');
      $('.unity-portal .upper-header').slideUp();
    } else {
      $('.portal .header').css('border-width', 8);
      $('.portal .upper-header').fadeIn();
      $('.portal .main-header').fadeIn();
      $('.portal .nav-main').css('box-shadow', 'none');
      $('.unity-portal .navbar').removeClass('white-nav').css('box-shadow', 'none');
      $('.unity-portal .upper-header').fadeIn();
    }
  });
});

function fileFieldStyle() {
  $('input[type=file]').filestyle({size: 'md', input: false, buttonText: 'Pesquisar...'});
}

function applyInputMasks() {
	$('input.cpf').mask('999.999.999-99');
	$('input.cnpj').mask('99.999.999/9999-99');
	$('input.zipcode').mask('99999-999');
	$('input.date').mask('99/99/9999');
  $('input.time').mask('99:99');
  $('input.datetime').mask('99/99/9999 99:99:99');
  $('input.phone').mask('(99) 9999-9999?9');
  $('input.weight').mask('9,999');
  $('input.size').mask('9,99');
  $('input.percent').mask('999,99');
  $('input.currency').maskMoney({ thousands:'.', decimal:',' });
  $('input.phone').blur(function(event) { var target, phone, element; target = (event.currentTarget) ? event.currentTarget : event.srcElement; phone = target.value.replace(/\D/g, ''); element = $(target); element.unmask(); if(phone.length > 10) { element.mask('(99) 99999-999?9');} else { element.mask('(99) 9999-9999?9'); }});
  $('.dpicker input').datepicker({ format: 'dd/mm/yyyy' });

  return true;
};

function getZipCode(prefix) {
  var zipcode = $('.zipcode:first').val().replace('-', '');
  if(zipcode.length >= 8) {
    $.ajax({
      url: 'https://viacep.com.br/ws/'+zipcode+'/json', dataType: 'json', cache: true,
      beforeSend: function() {
        $('.zipcode:first').css({background: 'white url("/assets/loader.gif") no-repeat center right 6px', backgroundSize: '16px'});
      },
      success: function(data) {
        $('.zipcode:first').css({background: 'white url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEIAAABCCAYAAADjVADoAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MUUzNzdFOERFMjM1MTFFNTkxNEZFMTFBNkRGOUVGOTYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MUUzNzdFOEVFMjM1MTFFNTkxNEZFMTFBNkRGOUVGOTYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoxM0VDNEFDRUUxQzMxMUU1OTE0RkUxMUE2REY5RUY5NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoxRTM3N0U4Q0UyMzUxMUU1OTE0RkUxMUE2REY5RUY5NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PjYUw3sAAAMRSURBVHja7JvfbeowFMbdqO+wwc0GsEH7gsTbTScgd4JLJwA2aCconaDwFikvMEHDBukGMAG1Y0elKIl9wrHjWP4kUgkCcX72d/4Yenc+n4kXIYFH4EF4EB6EB+FBKOv+5k9Ik27vYDKtGtOAHpf0Ma54R0bf84wPwjalyQM9rukjrITAATlujTRZ0OOuAcIjXQ0nPdawAwCzwqa40Wqx1+I6CG6ASJORWAXDWgiT6ZPrWWMmlnwdhKxYCY6nzzcRFEkDhMcmO/TdGgMBIMKC0EcQAxEPxg3nHAtIAAh9s8ZIzLQMAlsJX+YrS3MQmjJDKZYiD672GhAIW1ebrr+KENYUwrur3edMVIQyCKyJ+udqGz6T1Ai/g6Oj+xGqEAi0VugTiAcABFY6H7AubBOIkYgJKmKw3jEvHlgEQSU7lOXzHHsAQc8gHIUlTq6BGCimyFJzzLgAA8F2f9Lkj8YGKlQ8f4MdF9RB8C2wXeHLNJkhX3sjaaAulRPFDRZ8ED8QxmLprulzH+L5W/UGLIQiHXFBDuI3hOsBZWLLvK3+A2d3qSsuqKyIps2PsHidb523aaJeAOezVLkyEbWDBk/KZypNPulf1UA6AlSNZaqMTKWvoKF8VYExFrMmC6TQNFla4qtbELyRUZ2NoZjpD3HDdRkiBIyLWfPVZEFTnzX4lheklI3E6rgOpAtghjBqCbU6YjJ9FbOjqlCcv7gIjkvgmLSU0DLdK8503sLfEdAOpYW2pAPJS2xYvLgOpEOgJWLSkdSarsl0D8z/bTTvwhLw7pP/yiTTNI6dzoZKRxseiyWMqU4t0Q4ET6lL5DEYLZzwNmZ4St0gXT8zXTjhgcC1SEwsUTsQPKXGCJY49BsEh7G9IaXmBtKxIRA/s5q3tMTJHRDtLMIC7Z5YJozt/D0gpR6Jhi9nbAHBtFKsOl9sqBl0glBJhTkxtP/YNQhZ1RkTi4X9ld+qJotYGSB1gqiaeWsDpG4Q13sX1gZI3SDKQutoe4C8lK4fnGL0IkZ15/8lWq81PAgPwoPwIDwIF/UtwAAJwMG/5vzOHwAAAABJRU5ErkJggg==") no-repeat center right 6px', backgroundSize: '22px'});
        $('#'+prefix+'_street').val(data.logradouro); $('#'+prefix+'_district').val(data.bairro); $('#'+prefix+'_city').val(data.localidade); $('#'+prefix+'_state').val(data.uf); $('#'+prefix+'_number').focus();
      },
      error: function(data) {
        $('.zipcode:first').css({background: 'white url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEIAAABCCAYAAADjVADoAAACT0lEQVR4Xu3agU3DMBAFULoBbAATsEI3gA3oCIzCCLABbNAVmAA2gBE4g09KoyT23f+XFOkqRU2lxLGf72wnze4iP78Cu3T4E0iIGgkJkRCng0JGREZERsTkRJmpkamRqbFKatzKVY6yPcr2ErRqfZByn2Tby/bOugZzjFCEy1q5QwBGQXiu5X8zMVgQYwTtKCbGEEHLp2EwIOYQmBhTCFQMFKKFwMBYQqBhIBC9CAhGDwIFwwthRfBgWBBgDA+EF8GC4UGAMKwQKEIPBoLgxrBCfMiVrkmLmKmplYGg1fuUnZveulohWBExFRlMBPP6wgpRGhCBUcrVFWNvJ84dZ0YoBXkgIjDQxrvHBj3RC3GOGK5IYECcEwaEgKTGMJTZY4Y1TWAEFsSWkUFBYEJsgUFDYEOsiUFFiIBYA4OOEAURiRGCEAkRgRGGEA3BvHfQKfUgOyFPx5GV5dJ8H4EQihEBEYkQhsGGWAMhBIMJsSYCHYMFsQUCFYMBsSWCYtzLzpv1bm14PApxDgilPfAaA4G4kwq8Ir1APhfC8EIwn0GUBpSP/ouO+LgxPBBshH1t+XFLDCtEBIK+7BFZdjPKrBBfwb3GxrhqCtQDrBCMWaKVxywM0w2aFaL4IRgtBO1AFMOEUC7qgfBi9CKgGGYEBMKKYUXwYrgQUIheDC+CFcONwIBoYaAIvRgQAgtiDoOF0MKAEZgQYww2whwGBYENoRj014NVoX6HvObsnT5Hdfv/PxOi9mFCJMRpOmdEZERkRExOcZkamRqZGpkaS+vfH9T+q0NoDTTRAAAAAElFTkSuQmCC") no-repeat center right 6px', backgroundSize: '22px'});
        $('#'+prefix+'_street').val(''); $('#'+prefix+'_number').val(''); $('#'+prefix+'_district').val(''); $('#'+prefix+'_city').val(''); $('#'+prefix+'_state').val(''); $('#'+prefix+'_zipcode').focus();
      }
    });
  }
};
function CPFCNPJMask(o, f) { v_obj = o; v_fun = f; setTimeout('inputMask()', 1);}
function inputMask() { v_obj.value = v_fun(v_obj.value) }
function FormatCPFCNPJ(v) {
  v = v.replace(/\D/g, "");
  if (v.length <= 11) {
    v = v.replace(/(\d{3})(\d)/, "$1.$2");
    v = v.replace(/(\d{3})(\d)/, "$1.$2");
    v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2")
  } else {
    v = v.replace(/^(\d{2})(\d)/, "$1.$2");
    v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
    v = v.replace(/\.(\d{3})(\d)/, ".$1/$2");
    v = v.replace(/(\d{4})(\d)/, "$1-$2")
  }
  return v
}

function numberToCurrency(number, decimals, dec_point, thousands_sep) {
	var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
	var d = dec_point == undefined ? "," : dec_point;
	var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
	var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function number_format(number, decimals, dec_point, thousands_sep) {
	var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
	var d = dec_point == undefined ? "," : dec_point;
	var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
	var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}
