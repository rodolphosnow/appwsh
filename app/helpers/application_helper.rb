# encoding: utf-8
module ApplicationHelper

  def week_days
    %w(SEG TER QUA QUI SEX SÁB DOM)
  end

end
