source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

#ruby '2.3.2'

gem 'rails', '~> 5.0.2'
gem 'pg', '~> 0.18'
gem 'puma'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'jquery-rails'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'

gem 'devise'
gem 'simple_form'
gem 'friendly_id'
gem 'slim'
gem 'kaminari'
gem 'cocoon'
gem 'bcrypt', '~> 3.1.7'
gem 'paperclip', '~> 5.0.0'
gem 'aws-sdk', '~> 2.3'
gem 'rack-attack'
gem 'cpf_cnpj'
gem 'fcm'
gem 'phonelib'
gem 'validates_timeliness'
gem 'delorean'
gem 'geocoder'
gem 'brazilian-rails'
gem 'iugu'

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'letter_opener'
  gem 'rails-erd'
end

group :development, :test do
  gem 'byebug', platform: :mri
  gem 'rspec-rails'
  gem 'guard'
  gem 'guard-rspec'
end

group :test do
  gem 'simplecov'
  gem 'database_cleaner', '~> 1.0.1'
  gem 'launchy', '~> 2.3.0'
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'shoulda-matchers', git: 'https://github.com/thoughtbot/shoulda-matchers.git', branch: 'rails-5'
  gem 'capybara'
  gem 'rails-controller-testing'
  gem 'poltergeist'
end

group :production do
  gem 'rails_12factor'
  gem 'unicorn'
end
